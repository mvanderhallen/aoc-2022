{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (liftM)

  import Data.List (sort)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  signed :: Parser Integer
  signed = (*(-1)) <$> (symbol "-" *> natural)
    <|> natural

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p


  data Command = Noop | Add Integer deriving (Show, Eq)

  parseCommands :: Parser [Command]
  parseCommands = liftM concat $ many1 $ lexeme parseCommand

  parseCommand :: Parser [Command]
  parseCommand = lexeme $ do
    return [Noop]  <* symbol "noop"
    <|> (\x -> [Noop,Add x]) <$> (symbol "addx" *> signed)

  perform :: Command -> Integer -> Integer
  perform (Noop) i = i
  perform (Add v) i = i + v

  performAll :: [Command] -> [Integer]
  performAll cms= scanl (flip perform) 1 $ cms

  assignment1 cms = sum $ map (\x->(results!!x) * (fromIntegral x+1)) [19,59,99,139,179,219] --[18,58,98,138,178,218]
    where
      results = performAll cms

  assignment2 cms = format $ zipWith (drawPixel) [0..] middleOfSprite
    where
      middleOfSprite = performAll cms
      drawPixel = (\crt sprite -> if (abs (sprite - (crt `mod` 40))) <= 1 then '#' else ' ')
      format [] = []
      format s = take 40 s ++ ['\n'] ++ format (drop 40 s)

  instance Puzzle.Puzzle [Command] where
    parseInput = parseCommands
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @[Command]
