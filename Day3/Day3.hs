{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM)
  import Criterion.Main
  import Data.Char
  import Data.List (splitAt, intersect)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  parseRucksack :: Parser String
  parseRucksack = lexeme $ many1 pChar
    where
      pChar = oneOf $ "abcdefghijklmnopqrstuvwxyz" ++ map toUpper "abcdefghijklmnopqrstuvwxyz"

  findCommon :: String -> Char
  findCommon x = head $ uncurry intersect $ splitAt (length x `div` 2) x

  priority :: Char -> Int
  priority x
    | isUpper x = ord x - 38
    | isLower x = ord x - 96

  assignment1 :: FilePath -> IO Int
  assignment1 fp = do
    rucksacks <- getAOCInput (many parseRucksack) fp :: IO [String]
    return $ sum $ map (priority . findCommon) rucksacks

  take3 :: [a] -> [(a,a,a)]
  take3 [] = []
  take3 input = (convert $ take 3 input) : (take3 $ drop 3 input)
    where
      convert = (\[a,b,c] -> (a,b,c))

  findCommon' :: Eq a => ([a],[a],[a]) -> [a]
  findCommon' (a,b,c) = intersect c $ intersect a b

  assignment2 :: FilePath -> IO Int
  assignment2 fp = do
    rucksacks <- getAOCInput (many parseRucksack) fp :: IO [String]
    return $ sum $ map (priority . head . findCommon') $ take3 rucksacks

  main :: IO ()
  main = do
    args <- getArgs
    if (length $ filter (=="benchmark") args) == 1 then do
      withArgs (drop 1 args) $ criterion
    else do
      assignment1 "input.txt" >>= print
      assignment2 "input.txt" >>= print

  criterion :: IO ()
  criterion = defaultMain
    [
    bgroup "assignment1" $ [bench "" $ whnfIO (assignment1 "input.txt")],
    bgroup "assignment2" $ [bench "" $ whnfIO (assignment2 "input.txt")]
    ]
