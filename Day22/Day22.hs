{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle
  import Debug.Trace

  type PuzzleType = ([[CellType]], [Either Int Rotation])
  data Dir = Up | Down | L | R deriving (Eq, Show, Ord)
  data Rotation = CCW | CW deriving (Eq, Show, Ord)
  data CellType = Open | None | Wall deriving (Eq, Show, Ord)

  inputParser :: Parser ([[CellType]], [Either Int Rotation])
  inputParser = do
    field <- parseField
    whiteSpace
    inst <- many1 parseInstruction :: Parser [Either Int Rotation]
    return (field, inst)
    where
      parseInstruction = Left <$> int
                      <|>  oneOf "R" *> return (Right CW)
                      <|>  oneOf "L" *> return (Right CCW)
      parseField = many1 $ parseRow <* newline
      parseRow = many1 parseCell
      parseCell =  oneOf " " *> return None
               <|> oneOf "." *> return Open
               <|> oneOf "#" *> return Wall

  step :: [[CellType]] -> Either Int Rotation -> (Int, Int, Dir) -> (Int, Int, Dir)
  step _ (Right CCW) (x,y,Up)   = (x,y,L)
  step _ (Right CCW) (x,y,L)    = (x,y,Down)
  step _ (Right CCW) (x,y,Down) = (x,y,R)
  step _ (Right CCW) (x,y,R)    = (x,y,Up)
  step _ (Right CW) (x,y,Up)    = (x,y,R)
  step _ (Right CW) (x,y,R)     = (x,y,Down)
  step _ (Right CW) (x,y,Down)  = (x,y,L)
  step _ (Right CW) (x,y,L)     = (x,y,Up)
  step _ (Left 0)   (x,y,dir)   = (x,y,dir)
  step m (Left s)   (x,y,dir)   = let (x',y') = move (x,y) dir in
    case m !! y' !! x' of
      Wall -> (x,y,dir)
      Open -> step m (Left (s-1)) (x',y',dir)
      None -> trace (show (x,y,dir) ++ " " ++ show (x',y')) $ undefined

  move :: (Int, Int) -> Dir -> (Int, Int)
  move (x,y) R
    | ranges 0 y 50 && x == 149 = (50,y)
    | ranges 50 y 100 && x == 99 = (50, y)
    | ranges 100 y 150 && x == 99 = (0,y)
    | ranges 150 y 200 && x == 49 = (0,y)
  move (x,y) L
    | ranges 0 y 50 && x == 50 = (149,y)
    | ranges 50 y 100 && x == 50 = (99, y)
    | ranges 100 y 150 && x == 0 = (99,y)
    | ranges 150 y 200 && x == 0 = (49,y)
  move (x,y) Up
    | ranges 0 x 50 && y == 100 = (x,199)
    | ranges 50 x 100 && y == 0 = (x,149)
    | ranges 100 x 150 && y == 0 = (x,49)
  move (x,y) Down
    | ranges 0 x 50 && y == 199 = (x,100)
    | ranges 50 x 100 && y == 149 = (x,0)
    | ranges 100 x 150 && y == 49 = (x,0)
  move (x,y) Up = (x,y-1)
  move (x,y) Down = (x,y+1)
  move (x,y) L = (x-1,y)
  move (x,y) R = (x+1,y)

  ranges x y z = x <= y && y < z

  compute :: (Int, Int, Dir) -> Int
  compute (x,y,dir) = (x+1)*4+(y+1)*1000 + score dir
    where
      score R = 0
      score Down = 1
      score L = 2
      score Up = 3

  assignment1 :: PuzzleType -> _
  assignment1 input = compute $ foldl go (50,0,R) $ snd input
    where
      go st inst = step (fst input) inst st

  move' :: (Int, Int) -> Dir -> (Int, Int, Dir)
  move' (x,y) R
    | ranges 0 y 50 && x == 99 = (100,y,R) -- 1
    | ranges 0 y 50 && x == 149 = (99,99+(50-y), L) --2
    | ranges 50 y 100 && x == 99 = (100+(y-50),49, Up) -- 3
    | ranges 100 y 150 && x == 49 = (50,y,R) -- 4
    | ranges 100 y 150 && x == 99 = (149, 149-y, L) -- 5
    | ranges 150 y 200 && x == 49 = (y-100,149,Up) -- 6
  move' (x,y) L
    | ranges 0 y 49 && x == 50 = (0,149-y,R) -- 1
    | ranges 0 y 49 && x == 100 = (99,y,L) -- 2
    | ranges 50 y 100 && x == 50 = (y-50,100,Down) -- 3
    | ranges 100 y 150 && x == 0 = (50,149-y,R) -- 4
    | ranges 100 y 150 && x == 50 = (49,y, L) -- 5
    | ranges 150 y 200 && x == 0 = (y-100,0,Down) -- 6
  move' (x,y) Up
    | ranges 50 x 100 && y == 0 = (0, 100+x,R) -- 1
    | ranges 100 x 150 && y == 0 = (x-100,199,Up) -- 2
    | ranges 50 x 100 && y == 50 = (x,49, Up) -- 3
    | ranges 0 x 50 && y == 100 = (50,x+50,R) -- 4
    | ranges 50 x 100 && y == 100 = (x,99,Up) -- 5
    | ranges 0 x 50 && y == 150 = (x,149,Up) -- 6
  move' (x,y) Down
    | ranges 50 x 100 && y == 49 = (x, 50, Down) -- 1
    | ranges 100 x 150 && y == 49 = (99,x-50,L) -- 2
    | ranges 50 x 100 && y == 99 = (x,100, Down) -- 3
    | ranges 0 x 50 && y == 149 = (x,150,Down) -- 4
    | ranges 50 x 100 && y == 149 = (49,x+100,L) -- 5
    | ranges 0 x 50 && y == 199 = (x+100,0,Down) -- 6
  move' (x,y) Up = (x,y-1,Up)
  move' (x,y) Down = (x,y+1,Down)
  move' (x,y) L = (x-1,y,L)
  move' (x,y) R = (x+1,y,R)

  step' :: [[CellType]] -> Either Int Rotation -> (Int, Int, Dir) -> (Int, Int, Dir)
  step' _ (Right CCW) (x,y,Up)   = (x,y,L)
  step' _ (Right CCW) (x,y,L)    = (x,y,Down)
  step' _ (Right CCW) (x,y,Down) = (x,y,R)
  step' _ (Right CCW) (x,y,R)    = (x,y,Up)
  step' _ (Right CW) (x,y,Up)    = (x,y,R)
  step' _ (Right CW) (x,y,R)     = (x,y,Down)
  step' _ (Right CW) (x,y,Down)  = (x,y,L)
  step' _ (Right CW) (x,y,L)     = (x,y,Up)
  step' _ (Left 0)   (x,y,dir)   = (x,y,dir)
  step' m (Left s)   (x,y,dir)   = let (x',y',dir') = move' (x,y) dir in
    case m !! y' !! x' of
      Wall -> (x,y,dir)
      Open -> step' m (Left (s-1)) (x',y',dir')
      None -> trace (show (x,y,dir) ++ " " ++ show (x',y')) $ undefined

  assignment2 :: PuzzleType -> _
  assignment2 input = compute $ foldl go (50,0,R) $ snd input
    where
      go st inst = step' (fst input) inst st

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
