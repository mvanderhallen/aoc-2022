{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State, between)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle
  import Data.List (findIndex, sort, partition)
  import Data.Maybe (fromJust)
  import Control.Monad
  import Control.Monad.State
  import Control.Monad.Identity

  data Dir = Up | Down | L | R deriving (Eq, Show, Ord)

  type PuzzleType = (Config, [(Int, Int, Dir)])

  inputParser :: Parser PuzzleType
  inputParser = do
    start <- parseBorder (-1)
    tornadoes <- many1 parseTornadoes
    let torn' = concat $ zipWith (coords) [0..] tornadoes
    stop <- parseBorder (length tornadoes)
    return $ (MkConfig start stop (length $ tornadoes!!0) (length tornadoes), sort torn')
    where
      parseTornadoes = lexeme $ try $ do
        symbol "#"
        tornadoes <- many1 $ oneOf "^v<>."
        symbol "#"
        return tornadoes
      parseBorder n = lexeme $ do
        symbol "#"
        ix <- findIndex (=='.') <$> (many1 $ oneOf "#.")
        return (fromJust ix,n)
      coords n s = foldl (go n) [] $ zip [0..] s
      go n ls (ix, v)
        | v == '.' = ls
        | v == '^' = (ix,n, Up):ls
        | v == 'v' = (ix,n, Down):ls
        | v == '>' = (ix,n,R):ls
        | v == '<' = (ix,n,L):ls

  data Config = MkConfig { start :: (Int, Int), stop :: (Int, Int), width :: Int, height :: Int} deriving (Show, Eq, Ord)

  data PlanContext = MkPlanContext {startPos :: (Int, Int), stopPos :: (Int, Int), fieldWidth :: Int, fieldHeight :: Int, nbTornadoes :: Int, tornadoesOf :: Int -> [(Int, Int)] }

  instance Show PlanContext where
    show (MkPlanContext s e w h t _) = concat $ [show s, " -> ", show e, " [", show w , " x " , show h , "]" , " (", show t, ")"]

  mkContext :: Config -> [(Int, Int, Dir)] -> PlanContext
  mkContext cfg tornadoes = MkPlanContext (start cfg) (stop cfg ) (width cfg) (height cfg) (length $ tornadoPositions cfg tornadoes) (\i -> (tornadoPositions cfg tornadoes) !! i)

  moveTornadoes :: Config -> [(Int, Int, Dir)] -> [(Int, Int, Dir)]
  moveTornadoes cfg = sort . foldr moveTornado []
    where
      moveTornado :: (Int, Int, Dir) -> [(Int, Int, Dir)] -> [(Int, Int, Dir)]
      moveTornado (x,y,Up)   acc = (x,(y-1) `mod` height cfg,Up) : acc
      moveTornado (x,y,Down) acc = (x,(y+1) `mod` height cfg,Down) : acc
      moveTornado (x,y,L)    acc = ((x-1) `mod` width cfg,y,L) : acc
      moveTornado (x,y,R)    acc = ((x+1) `mod` width cfg,y,R) : acc

  tornadoPositions :: Config -> [(Int, Int, Dir)] -> [[(Int, Int)]]
  tornadoPositions cfg t = map (map (\(x,y,d) -> (x,y))) $
                           scanl (\t _ -> moveTornadoes cfg t) t [2..lcm (width cfg) (height cfg)]

  between :: Int -> Int -> Int -> Bool
  between from v to = and [from <= v, v < to]

  branch :: PlanContext -> ((Int, Int), Int) -> [((Int, Int),Int)]
  branch ctxt ((x,y), tornadoIdx) = do
    let nextTornadoIdx = (tornadoIdx + 1) `mod` nbTornadoes ctxt
    (x',y') <- [(x',y') | (x',y')<-[(x,y), (x+1,y),(x-1,y),(x,y+1),(x,y-1)],
                          validPos ctxt (x',y')]
    if (x',y') `elem` (tornadoesOf ctxt nextTornadoIdx) then
      []
    else
      return ((x',y'), nextTornadoIdx)

  search :: PlanContext -> [(Int,((Int, Int), Int))] -> State [((Int, Int), Int)] (Int,((Int, Int), Int))
  search ctxt ((s,(pos,tix)):ls)
    | pos == stopPos ctxt = return (s,(pos, tix))
    | otherwise = do
      forbidden <- get
      let options = [(s+1, option) | option<- branch ctxt (pos,tix), not $ elem option forbidden, not $ elem option $ map snd ls]
      modify ((pos,tix):)
      let (before, after) = partition ((<(s+1)) . fst) ls
      search ctxt (before ++ options ++ after)


  validPos :: PlanContext -> (Int, Int) -> Bool
  validPos ctxt (x,y)
    | (x,y) == startPos ctxt = True
    | (x,y) == stopPos ctxt =  True
    | otherwise = (between 0 x (fieldWidth ctxt)) && (between 0 y (fieldHeight ctxt))


  assignment1 :: PuzzleType -> Int
  assignment1 input = let ctxt = mkContext (fst input) (snd input) in
    fst $ evalState (search ctxt [(0,(startPos ctxt,0))]) []

  assignment2 :: PuzzleType -> Int
  assignment2 input = runIdentity $ do
    let ctxt = mkContext (fst input) (snd input)
    let (s1,(pos,tix))= evalState (search ctxt [(0,(startPos ctxt,0))]) []
    let (s2,(pos',tix'))= evalState (search (ctxt {startPos = stopPos ctxt, stopPos = startPos ctxt}) [(0,(pos,tix))]) []
    let (s3,(pos'', tix'')) = evalState (search ctxt [(0,(pos',tix'))]) []
    return $ s1+s2+s3

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
