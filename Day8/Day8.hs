{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, transpose)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  parseForest :: Parser [[Int]]
  parseForest = many1 $ lexeme $ many1 (value <$> digit)
    where
      value c = read [c]

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  leftInvariant :: [(Int, Int)] -> (Int, Int) -> [(Int, Int)]
  leftInvariant [] tree = [tree]
  leftInvariant q@((h1,p1):left) (h2, p2)
    | h2 <= h1 = q
    | otherwise = (h2,p2):q

  rightInvariant :: [(Int, Int)] -> (Int, Int) -> [(Int, Int)]
  rightInvariant rs tree = tree : dropWhile (<tree) rs

  visibleOnAxis :: [Int] -> _
  visibleOnAxis (x:xs)= nub $ map (snd) $ uncurry (++) $ foldl go ([(x,0)],[]::[(Int,Int)]) $ zip xs [1..]
    where
      go (ls,rs) tree = (leftInvariant ls tree, rightInvariant rs tree)

  assignment1 :: [[Int]] -> Int
  assignment1 forest = length $ nub $ concat $ visibleHorizontal ++ visibleVertical
    where
      visibleHorizontal = map (\(r,trees) -> map (r,) $ visibleOnAxis trees) $ zip [0..] forest
      visibleVertical = map (\(c,trees) -> map (,c) $ visibleOnAxis trees) $ zip [0..] $ transpose forest

  scoreTreehousePosIn :: (Int, Int) -> [[Int]] -> Int -- Int
  scoreTreehousePosIn (x,y) forest = foldl1 (*)
                  [length $ canSee $ map (\x -> (forest !! x !! y)) [x-1,x-2..0],
                   length $ canSee $ map (\x -> (forest !! x !! y)) [x+1..xMax],
                   length $ canSee $ map (\y -> (forest !! x !! y)) [y-1,y-2..0],
                   length $ canSee $ map (\y -> (forest !! x !! y)) [y+1..yMax]]
    where
      treeHeight :: Int
      treeHeight = forest !! x !! y
      xMax :: Int
      xMax = (length forest)-1
      yMax :: Int
      yMax = (length $ forest !! 0)-1
      canSee :: [Int] -> [Int]
      canSee [] = []
      canSee (x:xs)
        | x < treeHeight = x:canSee xs
        | otherwise = [x]

  assignment2 :: [[Int]] -> Int
  assignment2 forest = maximum $ [scoreTreehousePosIn (x,y) forest | x<-[0..xMax], y<-[0..yMax]]
    where
      xMax :: Int
      xMax = (length forest)-1
      yMax :: Int
      yMax = (length $ forest !! 0)-1


  instance Puzzle.Puzzle [[Int]] where
    parseInput = parseForest
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @[[Int]]
