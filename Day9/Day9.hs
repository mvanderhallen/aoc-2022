{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, guard)
  import Criterion.Main
  import Control.DeepSeq
  import Control.Monad.State

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)
  import Data.Set (Set)
  import qualified Data.Set as S

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  data Move = U Integer | D Integer | L Integer | R Integer deriving (Show, Eq)

  parseMove :: Parser Move
  parseMove =
        R <$> (symbol "R" *> natural)
    <|> L <$> (symbol "L" *> natural)
    <|> U <$> (symbol "U" *> natural)
    <|> D <$> (symbol "D" *> natural)

  parseMoves :: Parser [Move]
  parseMoves = many1 $ lexeme parseMove

  move :: ((Int, Int),(Int,Int)) -> Move -> State [(Int, Int)] ((Int,Int), (Int, Int))
  move curr mov = do
    modify (\s -> s ++ map fst results)
    return $ last results
    where
      results = scanl moveOne curr $ repl mov
      repl (D x) = replicate (fromIntegral x) (D 1)
      repl (U x) = replicate (fromIntegral x) (U 1)
      repl (L x) = replicate (fromIntegral x) (L 1)
      repl (R x) = replicate (fromIntegral x) (R 1)

  moveOne :: ((Int,Int),(Int, Int)) -> Move -> ((Int, Int), (Int, Int))
  moveOne (tp, hp) move  = (moveTail tp newHeadPos,newHeadPos)
    where
      newHeadPos = moveHead hp move

  moveTail :: (Int, Int) -> (Int, Int) -> (Int, Int)
  moveTail (tx,ty) (hx,hy)
    | abs (hx-tx) <= 1 && abs (hy-ty) <= 1 = (tx,ty)
    | hx == tx = (tx, correction ty hy)
    | hy == ty = (correction tx hx,ty)
    | otherwise = (correction tx hx, correction ty hy)
    where
      correction :: Int -> Int -> Int
      correction t h
        | h > t = t+1
        | h < t = t-1
        | otherwise = t

  moveHead :: (Int, Int) -> Move -> (Int,Int)
  moveHead (x,y) (U _) = (x,y-1)
  moveHead (x,y) (D _) = (x,y+1)
  moveHead (x,y) (L _) = (x-1, y)
  moveHead (x,y) (R _) = (x+1, y)

  assignment1 :: [Move] -> Int
  assignment1 moves = length $ nub $ execState (foldM move ((0,0),(0,0)) moves) []

  move' :: [(Int,Int)] -> Move -> State [(Int, Int)] [(Int, Int)]
  move' curr mov = do
    modify (\s -> s ++ map last results)
    return $ last results
    where
      results = scanl moveOne' curr $ repl mov
      repl (D x) = replicate (fromIntegral x) (D 1)
      repl (U x) = replicate (fromIntegral x) (U 1)
      repl (L x) = replicate (fromIntegral x) (L 1)
      repl (R x) = replicate (fromIntegral x) (R 1)

  moveOne' :: [(Int,Int)] -> Move -> [(Int,Int)]
  moveOne' (h:tails) move = positions
    where
      newHeadPos = moveHead h move
      positions = scanl (flip moveTail) newHeadPos tails

  assignment2 :: [Move] -> Int
  assignment2 moves = length $ nub $ execState (foldM move' (replicate 10 (0,0)) moves) []

  instance Puzzle.Puzzle [Move] where
    parseInput = parseMoves
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @[Move]
