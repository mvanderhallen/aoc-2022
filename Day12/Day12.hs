{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Data.Char (ord)
  import Data.List (findIndices)
  import Control.Monad.State

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type Heightmap = [[Char]]

  inputParser = many1 $ lexeme $ many1 $ oneOf "abcdefghijklmnopqrstuvwxyzSE"

  type Dijkstra a = State ([(Int, Int)], [((Int,Int), (Int,Int))]) a
  type PrioQueue k v = [(k,v)]
  type DijkstraBorder = PrioQueue Int (Int,Int)

  runDijkstra :: Dijkstra a -> (a, ([(Int, Int)], [((Int, Int), (Int, Int))]))
  runDijkstra = flip runState ([],[])

  evalDijkstra :: Dijkstra a -> a
  evalDijkstra = flip evalState ([],[])

  insertion :: (Ord k) => (k,v) -> PrioQueue k v -> PrioQueue k v
  insertion (k,v) [] = [(k,v)]
  insertion (k,v) ((k',v'):ls)
    | k' > k = (k,v):(k',v'):ls
    | otherwise = (k',v'): insertion (k,v) ls

  explore :: Heightmap -> DijkstraBorder -> Dijkstra DijkstraBorder
  explore hm ((k,v):border) = do
    candidates <- canVisit hm v
    forM candidates (visit v)
    return $ foldr (insertion) border $ map (k+1,) candidates

  visit :: (Int,Int) -> (Int, Int) -> Dijkstra ()
  visit from p = modify (\(v,l) ->(p:v, (from,p):l))

  canVisit :: Heightmap -> (Int, Int) -> Dijkstra [(Int,Int)]
  canVisit hm (r,c) = filterM (canVisitPred $ norm (hm!!r!!c)) $ possible
    where
      possible = neighbours hm (r,c)
      canVisitPred :: Char -> (Int, Int) -> Dijkstra Bool
      canVisitPred current (r',c') = do
        let nb = norm (hm!!r'!!c')
        if ord nb - ord current < 2 then
          gets (not . ((r',c') `elem`) . fst)
        else
          return False
      norm 'E' = 'z'
      norm 'S' = 'a'
      norm c = c

  neighbours :: Heightmap -> (Int,Int) -> [(Int, Int)]
  neighbours hm (x,y) = filter (\(x,y) -> x >= 0 && x < length hm && y >= 0 && y < length (hm!!0)) [(x-1,y), (x+1,y),(x,y-1), (x,y+1)]

  boundedDijkstra :: Int -> Heightmap -> (Char-> Bool) -> ((Int,Int)-> Bool) -> Dijkstra (Int, (Int,Int))
  boundedDijkstra maxDepth hm sp endCondition = do
    let startPos = findAll hm sp
    forM startPos (\x -> visit x x)
    loop $ map (0,) startPos
    where
      loop :: DijkstraBorder -> Dijkstra (Int, (Int, Int))
      loop b@((k,v):_)
        | endCondition v = return (k,v)
        | k <= maxDepth = explore hm b >>= loop
        | otherwise = return (k,v)

  findAll :: Heightmap -> (Char -> Bool) -> [(Int,Int)]
  findAll hm p = concat $ zipWith(\r -> map (r,) . findIndices p) [0..] hm

  isStartPos 'S' = True
  isStartPos _ = False

  isStartPos' 'S' = True
  isStartPos' 'a' = True
  isStartPos' _   = False

  isEndPos 'E' = True
  isEndPos _   = False

  assignment1 :: Heightmap -> (Int, (Int, Int))
  assignment1 hm = evalDijkstra $ boundedDijkstra 10000 hm isStartPos (\(r,c)->isEndPos $ hm !!r !!c)

  assignment2 :: Heightmap -> (Int, (Int, Int))
  assignment2 hm = evalDijkstra $ boundedDijkstra 10000 hm isStartPos' (\(r,c)->isEndPos $ hm !!r !!c)


  instance Puzzle.Puzzle Heightmap where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @Heightmap

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @Heightmap
