{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle

  import Data.List (findIndex, delete)
  import Data.Maybe (fromJust)

  type PuzzleType = [Int]
  type Segment a = (Int, [a])
  type SegmentedList a = (Int, [Segment a])
  type Index = Int

  find :: forall a. Eq a => SegmentedList a -> Index -> a -> Index
  find (orig, ls) ix target = bucketOffset + indexInBucket
    where
      bucket = ix `div` orig
      indexInBucket = fromJust $ findIndex (==target) $ snd $ ls !! bucket
      bucketOffset = head $ map fst $ drop bucket ls

  move :: forall a. Eq a => SegmentedList a -> a -> Index -> Index -> SegmentedList a
  move (orig, ls) val from to = (orig, updateSegment' ls)
    where
      boundary :: Int -> Int
      boundary b
        | from < b && to >= b = b - 1
        | to   < b && from >= b = b + 1
        | otherwise = b
      updateSegment' :: [Segment a] -> [Segment a]
      updateSegment' []       = []
      updateSegment' [(b,ls)] = [(boundary', insert' val $ delete' val ls)]
        where
          boundary' = boundary b
          delete' v l
            | b <= from = delete v l
            | otherwise = l
          insert' v l
            | boundary' <= to = insertAt (to-boundary') l v
            | otherwise = l
      updateSegment' ((b1,l1):(b2,l2):ls) =(boundary', (insert' val $ delete' val l1)):updateSegment' ((b2,l2):ls)
        where
          boundary' = boundary b1
          delete' v l
            | b1 <= from && from < b2 = delete v l
            | otherwise = l
          insert' v l
            | boundary' <= to && to < b2 = insertAt (to-boundary') l v
            | otherwise = l
      updateSegment :: Segment a -> [Segment a] -> [Segment a]
      updateSegment (boundary, ls) = undefined
      insertAt :: Int -> [a] -> a -> [a]
      insertAt ix ls val = (\(x,y) -> x ++ val:y) $ splitAt ix ls

  inputParser :: Parser PuzzleType
  inputParser = undefined

  segment :: forall a . Int -> [a] -> SegmentedList a
  segment size list = (size, go 0 list)
    where
      go start [] = []
      go start ls = let (l, lrem) = splitAt (size) ls in (start, l) : go (start+size) lrem

  assignment1 :: PuzzleType -> Int
  assignment1 input = extractCoord decrypted
    where
      decrypted = map snd $ concatMap snd $ snd $ foldl go segmented indexed
      indexed :: [(Int, Int)]
      indexed = zip [0..] input
      segmented = segment 1000 indexed
      go :: SegmentedList (Int, Int) -> (Index, Int) -> SegmentedList (Int,Int)
      go sl (ix,val) =
        move sl (ix, val) currIndex newIndex
        where
          currIndex = find sl ix (ix, val)
          newIndex = handle $ (currIndex + val) `mod` ((length input)-1)
          handle 0 = (length input) -1
          handle x
            | x == length input = 0
            | otherwise = x

  extractCoord :: Integral a => [a] -> a
  extractCoord ls =  sum $ map ((ls!!) . (`mod` (length ls))) $ (\x -> [x+1000, x+2000, x+3000]) $ fromJust $ findIndex (==0) ls

  assignment2 :: PuzzleType -> Integer
  assignment2 input' = extractCoord $ map snd $ foldl (\list _ -> round list) indexed [1..10]
    where
      input :: [Integer]
      input = map ((*811589153) . fromIntegral) input'
      indexed :: [(Index, Integer)]
      indexed = zip [0..] input
      len = fromIntegral $ length input
      round :: [(Index, Integer)] -> [(Index,Integer)]
      round list = do
        let segmented = segment 1000 list
        let bucketMap = zipWith (\v ix -> (v, ix)) list [0..]
        concatMap snd $ snd  $ foldl (go' bucketMap) segmented $ indexed
        where
          go' :: [((Index, Integer),Int)] -> SegmentedList (Index, Integer) -> (Index, Integer) -> SegmentedList (Index, Integer)
          go' bm sl (ix, val) = move sl (ix,val) currIndex newIndex
            where
              currIndex = find sl (fromJust $ lookup (ix,val) bm) (ix,val)
              newIndex = fromIntegral $ handle $ ((fromIntegral currIndex) + val) `mod` (len-1)
              handle :: Integer -> Integer
              handle 0 = len -1
              handle x
                | x == len = 0
                | otherwise = x

  instance Puzzle.Puzzle PuzzleType where
    parseInput = many1 $ lexeme $ signed int
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
