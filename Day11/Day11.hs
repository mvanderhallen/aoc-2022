{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, forM_)
  import Criterion.Main

  import Data.List (sort)

  import Data.STRef
  import Control.Monad.ST

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace
  int = fromIntegral <$> natural

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  data Expr = Old | Const Int | Mul Expr Expr | Add Expr Expr deriving (Show, Eq)
  data Monkey = Monkey Int [Int] Expr (Int,Int,Int) deriving (Show, Eq)

  class Eval a where
    eval :: Int -> a -> Int

  instance Eval Expr where
    eval x Old = x
    eval _ (Const x) = x
    eval x (Mul e1 e2) = (eval x e1) * (eval x e2)
    eval x (Add e1 e2) = (eval x e1) + (eval x e2)

  parseMonkey :: Parser Monkey
  parseMonkey = do
    monkeyNum <- symbol "Monkey " *> int <* symbol ":"
    items <- lexeme $ parseItems
    expr <- parseExpr
    test <- parseTestExpr
    return $ Monkey monkeyNum items expr test

  parseItems :: Parser [Int]
  parseItems = do
    symbol "Starting items: "
    int `sepBy` (symbol ",")

  parseTestExpr :: Parser (Int, Int, Int)
  parseTestExpr = do
    test <- lexeme $ symbol "Test: divisible by " *> int
    ifTrue <- lexeme $ symbol "If true: throw to monkey " *> int
    ifFalse <- lexeme $ symbol "If false: throw to monkey "*> int
    return (test, ifTrue, ifFalse)

  parseExpr :: Parser Expr
  parseExpr = do
    symbol "Operation: new = "
    e1 <- parseTerm
    op <- parseOperator
    e2 <- parseTerm
    return $ op e1 e2

  parseOperator :: Parser (Expr -> Expr -> Expr)
  parseOperator = return Mul <* symbol "*"
        <|> return Add <* symbol "+"

  parseTerm :: Parser Expr
  parseTerm = lexeme go
    where
      go = return Old <* symbol "old"
        <|> (Const <$> int)

  data MonkeyBusinessConfig = MkMonkeyBusinessConfig {whenBored :: Int -> Int, rounds :: Int}

  round :: forall s . MonkeyBusinessConfig -> [Monkey] -> [STRef s Int] -> [STRef s [Int]] -> ST s ()
  round config monkeys looks refs = do
      forM_ monkeys (processMonkey)
    where
      processMonkey :: Monkey -> ST s ()
      processMonkey m@(Monkey i _ _ _)= do
        itemsCurrentlyInPossession <- readSTRef (refs !! fromIntegral i)
        writeSTRef (refs !! fromIntegral i) []
        sequence_ $ map (doYourThing m) itemsCurrentlyInPossession
      doYourThing :: Monkey -> Int -> ST s ()
      doYourThing (Monkey i _ e (test,ifTrue, ifFalse)) item = do
        modifySTRef (looks!! fromIntegral i) (+1)
        if newVal `mod` test == 0
          then modifySTRef (refs!!fromIntegral ifTrue) (++[newVal])
          else modifySTRef (refs!!fromIntegral ifFalse) (++[newVal])
        where
          newVal = whenBored config (eval item e)

  runRoundsWith :: MonkeyBusinessConfig -> [Monkey] -> [[Int]] -> ST s [Int]
  runRoundsWith config monkeys items = do
    refPossessions <- sequence $ map newSTRef items
    refLooks <- sequence $ map newSTRef $ replicate (length monkeys) 0
    replicateM (rounds config) $ Main.round config monkeys refLooks refPossessions
    sequence $ map readSTRef refLooks

  assignment1 :: [Monkey] -> Int
  assignment1 monkeys =  foldl1 (*) $ take 2 $reverse $ sort $ runST $ runRoundsWith config monkeys initialPoss
    where
      config = MkMonkeyBusinessConfig (`div` 3) 20
      initialPoss = map (\(Monkey _ ip _ _) -> ip) monkeys

  assignment2 :: [Monkey] -> Int
  assignment2 monkeys =  foldl1 (*) $ take 2 $reverse $ sort $ runST $ runRoundsWith config monkeys initialPoss
    where
      -- To keep numbers manageable, map every worry level to a number that does not change the result of any test
      -- We can in fact achieve this by performing a mod operation with the product of all tests.
      config = MkMonkeyBusinessConfig (`mod` modulo) 10000
      initialPoss = map (\(Monkey _ ip _ _) -> ip) monkeys
      modulo = foldl1 (*) $ map (\(Monkey _ _ _ (t,_,_)) -> t) monkeys

  instance Puzzle.Puzzle [Monkey] where
    parseInput = many1 parseMonkey
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @[Monkey]
