{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeFamilies, TypeSynonymInstances, FlexibleInstances #-}
{-# LANGUAGE TypeApplications #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Control.Monad.Identity
  import Criterion.Main
  import Control.DeepSeq
  import Control.Monad.ST
  import Data.STRef
  import Data.Maybe (fromJust)
  import Debug.Trace

  import Data.List (sort, partition)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  data Command = CD String | LS deriving (Eq, Show)
  data OutputElement = Dir String | File Integer String deriving (Eq, Show)
  type Output = [OutputElement]

  parseCommand :: Parser Command
  parseCommand = symbol "$ " *> (CD <$> (symbol "cd" *>) name
    <|> return LS <* symbol "ls")

  parseOutputElement :: Parser OutputElement
  parseOutputElement = Dir <$> (symbol "dir" *>) name
      <|> File <$> natural <*> name

  name = many1 (letter <|> oneOf "./")

  parseInput :: Parser [(Command, Maybe Output)]
  parseInput = many1 $ (do
    command <- lexeme $ parseCommand
    case command of
      LS -> do
        output <- many1 $ lexeme $ parseOutputElement
        return (command, Just output)
      CD _ -> do
        return (command, Nothing))

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type family HKD f a where
    HKD Identity a = a
    HKD (ST s)   a = STRef s a
    HKD f        a = f a

  data FileSystemEntry m = DirectoryEntry (Maybe (HKD m (FileSystemEntry m))) [(String,HKD m (FileSystemEntry m))] | FileEntry Integer

  type ConcreteFileSystem = FileSystemEntry Identity

  instance Show ConcreteFileSystem where
    show (FileEntry i) = "File " ++ (show i)
    show (DirectoryEntry _ cs) = "Directory: " ++ show (map show cs)


  file :: Integer -> FileSystemEntry m
  file = FileEntry

  direct :: String -> [(String, ConcreteFileSystem)] -> ConcreteFileSystem
  direct name contents = DirectoryEntry Nothing contents

  createFromOutputElement :: forall s . STRef s (FileSystemEntry (ST s)) ->OutputElement -> ST s (String, (STRef s (FileSystemEntry (ST s))))
  createFromOutputElement parentRef (Dir name) = (name, ) <$> (newSTRef $ DirectoryEntry @(ST s) (Just parentRef) [])
  createFromOutputElement parentRef (File size name) = (name, ) <$> (newSTRef $ FileEntry size)

  run :: STRef s (FileSystemEntry (ST s)) -> ST s ConcreteFileSystem
  run ref = do
    fileSystemEntry <- readSTRef ref
    case fileSystemEntry of
      (FileEntry i) -> return $ FileEntry i
      (DirectoryEntry parent ls) -> do
        DirectoryEntry Nothing <$> mapM (\(name, ref) -> (name,)<$> run ref) ls

  process :: (Command, Maybe Output) -> (STRef s (FileSystemEntry (ST s))) -> ST s (STRef s (FileSystemEntry (ST s)))
  process (LS, Just output) ref = do
    (DirectoryEntry parent contents) <- readSTRef ref
    if length contents == 0
      then do
        contents' <- mapM (createFromOutputElement ref) output
        writeSTRef ref (DirectoryEntry parent $ contents')
        return ref
      else
        return ref
  process (CD "..", Nothing) ref = do
    (DirectoryEntry parent contents) <- readSTRef ref
    case parent of
      Just ref -> return ref
      Nothing -> return ref
  process (CD xs, Nothing) ref = do
    (DirectoryEntry parent contents) <- readSTRef ref
    case (lookup xs contents) of
      Just subFolderRef -> return subFolderRef
      Nothing -> do
        traceM (show xs ++ " | " ++ (show $ map fst contents))
        return undefined


  processAll :: [(Command, Maybe Output)] -> ST s (STRef s (FileSystemEntry (ST s)))
  processAll trace = do
    rootRef <- root
    finalRef <- foldM (flip process) rootRef trace
    returnUp finalRef

  returnUp :: STRef s (FileSystemEntry (ST s)) -> ST s (STRef s (FileSystemEntry (ST s)))
  returnUp ref = do
    (DirectoryEntry parent c) <- readSTRef ref
    case parent of
      Nothing -> return ref
      Just x  -> returnUp x

  root :: ST s (STRef s (FileSystemEntry (ST s)))
  root = newSTRef $ DirectoryEntry Nothing []

  sizeOf :: ConcreteFileSystem -> Integer
  sizeOf (FileEntry i) = i
  sizeOf (DirectoryEntry _ contents) = sum $ map (sizeOf . snd) contents

  countAll :: ConcreteFileSystem -> Integer
  countAll (FileEntry _) = 0
  countAll d@(DirectoryEntry _ contents)
    | sizeOf d <= 100000 = sizeOf d + sum (map (countAll . snd) contents)
    | otherwise = sum (map (countAll . snd) contents)

  allSizes :: ConcreteFileSystem -> [Integer]
  allSizes (FileEntry _) = []
  allSizes d@(DirectoryEntry _ contents) = sizeOf d : foldl (\acc (_,folder) -> acc ++ allSizes folder) [] contents

  instance Puzzle.Puzzle [(Command, Maybe Output)] where
    parseInput = parseInput
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  assignment1 trace = countAll (runST (processAll (tail trace) >>= run))

  assignment2 trace= head $ sort $ filter (>3598596) $ allSizes (runST (processAll (tail trace) >>= run))


  allNamesIn (FileEntry _) = []
  allNamesIn (DirectoryEntry _ contents) = map fst contents ++ (foldl (\acc (_,folder) -> acc ++ allNamesIn folder) [] contents)

  ------- Approach 2

  data OutputLine = CDLine String | FileLine Integer String | DirectoryLine String deriving (Eq, Show)

  parseI :: Parser [OutputLine]
  parseI = many1 $ lexeme go
    where go = (CDLine <$> (symbol "$ cd" *> lexeme name <* (optional $ try $ symbol "$ ls")))
            <|> DirectoryLine <$> (symbol "dir" *> name)
            <|> FileLine <$> natural <*> name

  data Directory = MkDirectoryInfo { dirName :: String, dirSize :: Integer, directories :: [Directory]} deriving (Show, Eq)

  type AssocList k v = [(k,v)]

  alterFirst :: forall k v . (Eq k) => k -> (Maybe v -> Maybe (k,v)) -> AssocList k v -> AssocList k v
  alterFirst key update [] = case update Nothing of
      Just (k,v) -> [(k,v)]
      Nothing -> []
  alterFirst key update ((k,v):ls)
    | key == k = case update (Just v) of
        Just (k',v') -> (k',v') : ls
        Nothing      -> ls
    | otherwise = (k,v) : alterFirst key update ls

  removeFirst :: forall k v . (Eq k) => k -> AssocList k v -> (Maybe (k,v), AssocList k v)
  removeFirst key [] = (Nothing,[])
  removeFirst key ((k,v):ls)
    | key == k = (Just (k,v), ls)
    | otherwise = (\(r,l) -> (r,(k,v):l)) $ removeFirst key ls

  process' :: [OutputLine] -> (String, Directory)
  process' = fromJust . fst . removeFirst "/" . foldr go [("", MkDirectoryInfo "" 0 [])]
    where
      go :: OutputLine -> AssocList String Directory -> AssocList String Directory
      go (FileLine i _) assoc = alterFirst "" (return . maybe emptyDir (\dir -> ("",dir {dirSize = dirSize dir + i}))) assoc
      go (DirectoryLine s) assoc = case removeFirst s assoc of
          (Just (k,v), ls) -> alterFirst "" (return . maybe emptyDir (\dir -> ("", dir {dirSize = dirSize dir + dirSize v, directories = v:directories dir}))) ls
          (Nothing, ls)    -> undefined
      go (CDLine s) assoc
        | s == ".." = assoc
        | otherwise =  ("", MkDirectoryInfo "" 0 []): alterFirst "" (fmap (\dir -> (s, dir {dirName = s}))) assoc
      emptyDir = ("", MkDirectoryInfo "" 0 [])

  findDirectories :: (Directory -> Bool) -> Directory -> [Directory]
  findDirectories cond d = if cond d
    then
        d : concatMap (findDirectories cond) (directories d)
    else
      concatMap (findDirectories cond) (directories d)

  assignment1' outputLines = sum $ map (dirSize) $ findDirectories (\d -> dirSize d <= 100000) $ rootDir
    where
      rootDir = snd $ process' outputLines

  assignment2' outputLines = head $ sort $ map (dirSize) $ findDirectories (\d -> dirSize d >= sizeNeeded) $ rootDir
    where
      rootDir = snd $ process' outputLines
      sizeNeeded = 30000000 - (70000000 - dirSize rootDir)

  instance Puzzle.Puzzle [OutputLine] where
    parseInput = parseI
    assignment1 = show . assignment1'
    assignment2 = show . assignment2'

  main :: IO ()
  main = Puzzle.doMain @[OutputLine]
