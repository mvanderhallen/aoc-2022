{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle
  import Data.Maybe (catMaybes)
  import Data.List (findIndices, sort)

  type PuzzleType = [(Int, Int)]
  type Rule = ((Int,Int) -> Bool) -> (Int, Int) -> Maybe (Int, Int)

  rule1 :: Rule
  rule1 check (x,y) = if and [check (x,y-1), check (x+1,y-1), check (x-1,y-1)] then Just (x,y-1) else Nothing

  rule2 :: Rule
  rule2 check (x,y) = if and [check (x,y+1), check (x+1,y+1), check (x-1,y+1)] then Just (x,y+1) else Nothing

  rule3 :: Rule
  rule3 check (x,y) = if and [check (x-1,y), check (x-1,y+1), check (x-1,y-1)] then Just (x-1,y) else Nothing

  rule4 :: Rule
  rule4 check (x,y) = if and [check (x+1,y), check (x+1,y+1), check (x+1,y-1)] then Just (x+1,y) else Nothing

  consider :: [Rule] -> [(Int, Int)] -> [((Int, Int), (Int, Int))]
  consider rules elves = foldr go [] $ elves
    where
      go pos@(x,y) acc
        | or $ map (`elem` elves) [(x,y-1), (x+1,y-1), (x+1,y), (x+1,y+1), (x,y+1), (x-1,y+1), (x-1,y), (x-1,y-1)] =  (pos, head $ catMaybes $ (++ [Just pos]) $ map (\r -> r (not . (`elem` elves)) pos) rules) : acc
        | otherwise = (pos, pos) : acc

  move :: [((Int, Int), (Int, Int))] -> [(Int, Int)]
  move intentions = foldr go [] intentions
    where
      go (from,to) acc = if 1 < (length $ findIndices ((== to) . snd) intentions) then
        from : acc
      else
        to : acc

  round :: [Rule] -> [(Int, Int)] -> Int -> [(Int, Int)]
  round rules elves n = move $ consider ruleSet elves
    where
      ruleSet = take 4 $ drop (n `mod` 4) rules ++ rules

  rounds :: [Rule] -> [(Int, Int)] -> Int -> [[(Int, Int)]]
  rounds rules elves nbRounds = scanl (\acc nb -> Main.round rules acc nb) elves [0..nbRounds-1]

  inputParser :: Parser PuzzleType
  inputParser = transform <$> (many1 $ lexeme $ many1 $ oneOf ".#")
    where
      transform :: [[Char]] -> [(Int, Int)]
      transform grid = concat $ zipWith (\column row -> map (,row) $ findIndices (=='#') column) grid [0..]

  assignment1 :: PuzzleType -> Int
  assignment1 input = ((xMax+1 - xMin) * (yMax +1 - yMin)) - (length input)
    where
      finalPos = last $ rounds [rule1, rule2, rule3, rule4] input 10
      xMin = minimum $ map fst finalPos
      xMax = maximum $ map fst finalPos
      yMin = minimum $ map snd finalPos
      yMax = maximum $ map snd finalPos

  assignment2 :: PuzzleType -> Int
  assignment2 input = go input 0
    where
      go :: [(Int, Int)] -> Int -> Int
      go elves roundNb = if result == elves then roundNb+1 else go result (roundNb+1)
        where result = sort $ Main.round [rule1, rule2, rule3, rule4] elves roundNb

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
