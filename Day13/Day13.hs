{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, liftM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, foldl', sortOn, findIndex)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [(Packet, Packet)]

  parsePacketPairs :: Parser [(Packet, Packet)]
  parsePacketPairs = many1 parsePacketPair
    where
      parsePacketPair = (,) <$> parsePacket <*> parsePacket

  parsePacket :: Parser Packet
  parsePacket = lexeme $ parseListOf
    <|> parseSingle
    where
      parseListOf = do
        symbol "["
        comparables <- sepBy parsePacket (symbol ",")
        symbol "]"
        return $ ListOf comparables
      parseSingle =
        Single <$> natural

  data Packet = ListOf [Packet] | Single Integer deriving (Show, Eq)

  instance Ord Packet where
    -- compare :: Packet -> Packet -> Ordering
    compare (Single l)      (Single r)      = compare l r
    compare (ListOf l)      (Single r)      = compare (ListOf l) (ListOf [Single r])
    compare (Single l)      (ListOf r)      = compare (ListOf [Single l]) (ListOf r)
    compare (ListOf [])     (ListOf _)      = LT
    compare (ListOf _)      (ListOf [])     = GT
    compare (ListOf (l:ls)) (ListOf (r:rs))
      | compare l r == EQ = compare (ListOf ls) (ListOf rs)
      | otherwise = compare l r

  assignment1 :: [(Packet, Packet)] -> [Integer]
  assignment1 = map (fst) . filter ((==LT) . snd) . zip [1..] . map (uncurry compare)

  assignment2 :: [(Packet, Packet)] -> Maybe Int
  assignment2 = findDecoderKey . sort . ((divider1 :) . (divider2 :)) . concatMap (\(p1,p2) -> [p1,p2])
    where
      divider1 = ListOf [ListOf [Single 2]]
      divider2 = ListOf [ListOf [Single 6]]
      findDecoderKey sorted = (\x y -> (x+1)*(y+1)) <$> findIndex (==divider1) sorted <*> (findIndex (== divider2) sorted)

  instance Puzzle.Puzzle [(Packet, Packet)] where
    parseInput = parsePacketPairs
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main = Puzzle.doMain @PuzzleType
