{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances, TypeFamilies #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, when)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, sortOn, findIndex)
  import Data.Maybe (fromJust)
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle
  import Control.Monad.ST
  import Control.Monad.Identity
  import Data.STRef

  data Room = MkRoom { name :: String, flowRate :: Int, connections :: [String]} deriving (Show, Eq)
  type PuzzleType = [Room]

  parseRooms :: Parser [Room]
  parseRooms = many1 $ lexeme parseRoom

  parseRoom :: Parser Room
  parseRoom = do
    symbol "Valve"
    name <- ident
    symbol "has flow rate="
    flow <- int
    (try (symbol "; tunnels lead to valves") <|> symbol "; tunnel leads to valve")
    connections <- sepBy ident (symbol ",")
    return $ MkRoom name flow connections

  data Valve = MkValve { valveName :: String, pressure :: Int } deriving (Show, Eq)

  select :: [a] -> [(a,[a])]
  select []     = []
  select [a]    = return (a,[])
  select (x:xs) = (x,xs) : (map ((x:) <$>) $ select xs)

  eval :: forall a. Score a => (Valve->Valve->Int) -> (a,Valve, Int) -> [Valve] -> [a]
  eval dist (score,_, _) [] = return score
  eval dist (score,valve, time) valves
   | time <= 0 = return score
   | otherwise = do
      let score' = score +++ compute valve time
      (valve',valves')  <- select $ sortOn (dist valve) valves
      let time' = time - (dist valve valve' + 1)
      eval dist (score', valve', time') valves'

  assignment1 :: [Room] -> Int
  assignment1 rooms = maximum $ eval dist (0, MkValve "AA" 0, 30) valves
    where
      valves = map toValve $ filter (\(MkRoom _ r _) -> r > 0) rooms
      toValve (MkRoom name flow _) = MkValve name flow
      dist = (\(MkValve v1 _) (MkValve v2 _) -> fromJust $ lookup v1 distances >>= lookup v2)
      distances = computeDistances rooms

  type DistanceMatrix m = [(String, [(String, HKD m Int)])]
  type family HKD f a where
    HKD Identity a = a
    HKD f        a = f a

  adjacencyMatrix :: forall s. [Room] -> ST s (DistanceMatrix (STRef s))
  adjacencyMatrix rooms = sequence [fmap (name from, ) $processRoom from | from <-rooms]
    where
      names = map name rooms
      processRoom :: Room -> ST s [(String, STRef s Int)]
      processRoom from = mapM (\to -> fmap (to,) $ newSTRef (if to `elem` (connections from) then 1 else if (name from == to) then 0 else 1000000)) names -- $ filter (/= name from) names

  computeDistancesInST :: forall s . [Room] -> (DistanceMatrix (STRef s)) -> ST s (DistanceMatrix (STRef s))
  computeDistancesInST rooms adj = sequence [update from to via|via <- names, from<-names, to<-names] *> return adj
    where
      names = map name rooms
      update :: String -> String -> String -> ST s ()
      update from to via= do
        let ref = fromJust (lookup from adj >>= lookup to)
        d <- readSTRef ref
        d1 <- readSTRef $ fromJust (lookup from adj >>= lookup via)
        d2 <- readSTRef $ fromJust (lookup via adj >>= lookup to)
        if (d > d1 + d2) then
          writeSTRef ref (d1+d2)
        else
          return ()

  computeDistances :: [Room] -> DistanceMatrix Identity
  computeDistances rooms = runST $ do
    adjacencyMatrix rooms >>= computeDistancesInST rooms >>= readAll
    where
      readAll :: forall s. DistanceMatrix (STRef s) -> ST s (DistanceMatrix Identity)
      readAll dist= sequence [fmap (from,) $ sequence [fmap (to,) $ readSTRef ref |(to, ref)<-distances]| (from, distances)<-dist]

  evalWithHelp :: forall a. Score a => (Valve->Valve->Int) -> (a, (Valve,Int), (Valve,Int), Int) -> [Valve] -> [a] --
  evalWithHelp dist (score,(valve,i),(valve2,i2), time) valves
   | time < 0 = return score
   | otherwise = do
      let score' = score +++ addToScore time (valve, i) +++ addToScore time (valve2, i2)
      (valve', time', valves')    <- pick time (valve, i)  (valve2, i2) valves
      (valve2', time2', valves'') <- pick time (valve2, i2) (valve', time') valves'
      if (valve' == valve2' && valveName valve' /= "AA") then
        evalWithHelp dist (score', (valve', max time2' time'), (MkValve "AA" 0, -100), max time2' time') valves''
      else
        evalWithHelp dist (score', (valve', time'), (valve2', time2'), max time' time2') valves''
      where
          pick :: Int -> (Valve, Int) -> (Valve, Int) -> [Valve] ->  [(Valve, Int, [Valve])]
          pick time (v, t) (v',t') valves
            | time > t = return (v,t,valves)
            | time == t = do
                let pickedWithArrivalTime = map (\(picked, otherValves) -> (picked, time-(dist v picked +1), otherValves)) $ select valves
                case filter (\(_,t,_) -> t >= 0) $ pickedWithArrivalTime of
                  [] -> if (time-(dist v v'+1)) > t' && (time-(dist v v'+1)) > 0 then
                          return (v', (time-(dist v v'+1)), valves)
                        else
                          return (MkValve "AA" 0, -100, valves)
                  picks -> picks

  addToScore :: forall a. Score a => Int -> (Valve, Int) -> a
  addToScore time (v,t)
    | time == t = compute v t
    | otherwise = Main.mempty
-- CAVEATS:
-- Valves out of reach for one player could be in reach for the other player
-- When one of the two has nothing left to do (because they opened the final unassigned valve), it might be better to let this player go to the valve assigned to the other. A case that could be better handled by the code than how it's handled now.

  class Score a where
    (+++) :: a -> a -> a
    compute :: Valve -> Int -> a
    mempty :: a

  infixl 1 +++

  instance Score Int where
    (+++) = (+)
    compute v t = pressure v * t
    mempty = 0

  instance Score String where
    (+++) = (++)
    compute v t = show v ++ " * " ++ show t ++ ", "
    mempty = ""

  assignment2 :: [Room] -> Int
  assignment2 rooms =  maximum $ evalWithHelp dist (0::Int, (MkValve "AA" 0, availableTime), (MkValve "AA" 0, availableTime), availableTime) valves
    where
      availableTime = 26
      valves = map toValve $ filter (\(MkRoom _ r _) -> r > 0) rooms
      toValve (MkRoom name flow _) = MkValve name flow
      dist = (\(MkValve v1 _) (MkValve v2 _) -> fromJust $ lookup v1 distances >>= lookup v2)
      distances = computeDistances rooms

  instance Puzzle.Puzzle PuzzleType where
    parseInput = parseRooms
    assignment1 = show . Main.assignment1
    assignment2 = show . Main.assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
