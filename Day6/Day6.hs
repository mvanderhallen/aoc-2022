{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeApplications, TypeSynonymInstances, FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Control.Monad.Identity
  import Criterion.Main
  import Data.List(nub, tails, findIndex, isPrefixOf)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  parseInput :: Parser String
  parseInput = many1 letter

  startOfPacket :: Eq a => Int -> [a] -> [a]
  startOfPacket n input
    | length (nub (take n input)) == n = take n input
    | otherwise = startOfPacket n $ tail input

  findSubstring :: Eq a => [a] -> [a] -> Maybe Int
  findSubstring pat str = (length pat+) <$> findIndex (isPrefixOf pat) (tails str)

  assignment1 :: String -> Maybe Int
  assignment1 content = findSubstring (startOfPacket 4 content) content

  assignment2 :: String -> Maybe Int
  assignment2 content = findSubstring (startOfPacket 14 content) content

  instance Puzzle.Puzzle String where
    parseInput = parseInput
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @String
