{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle
  import Data.Maybe (fromJust)

  type PuzzleType = [String]

  inputParser :: Parser PuzzleType
  inputParser = many1 $ lexeme $ many1 $ oneOf "012-="

  fromSNAFU :: String -> Integer
  fromSNAFU = foldl (\acc v -> (acc * 5) + (convert v)) 0
    where
      convert '0' = 0
      convert '1' = 1
      convert '2' = 2
      convert '-' = -1
      convert '=' = -2

  toSNAFU :: Integral a => a -> String
  toSNAFU = reverse . go
    where
      go :: Integral a => a -> String
      go 0 = ""
      go x = charOf m : go (d + carry m)
        where
          (d,m) = x `divMod` 5
          charOf :: Integral a => a -> Char
          charOf = fromJust . flip lookup [(0,'0'), (1,'1'), (2,'2'), (3,'='), (4,'-')]
          carry x = if x >= 3 then 1 else 0

  assignment1 :: PuzzleType -> String
  assignment1 = toSNAFU . sum . map fromSNAFU

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = assignment1
    assignment2 = const "No part 2 on the 25th."

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
