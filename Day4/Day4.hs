{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM)
  import Criterion.Main

  import Data.IntMap.Strict (IntMap)
  import qualified Data.IntMap.Strict as IntMap
  import Data.Vector ((!), accum, Vector, fromList)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type Range = (Integer,Integer)

  parseRange :: Parser Range
  parseRange = (,) <$> (natural <* symbol "-") <*> natural

  parsePair :: Parser (Range,Range)
  parsePair = (,) <$> (parseRange <* symbol ",") <*> parseRange

  overlapping :: (Range, Range) -> Bool
  overlapping (range1,range2) = check range1 range2 || check range2 range1
    where
      check :: Range -> Range -> Bool
      check (l1,r1) (l2, r2) = l1 <= l2 && r1 >= r2

  assignment1 fp = do
    pairs <- getAOCInput (many1 $ lexeme $ parsePair) fp
    return $ length $ filter overlapping pairs

  overlapping' :: (Range, Range) -> Bool
  overlapping' (range1, range2) = check range1 range2 || check range2 range1
    where
      check :: Range -> Range -> Bool
      check (l1,r1) (l2, r2) = ((l1 >= l2) && (l1 <= r2)) || ((r1 >= l2) && (r1 <= r2))

  assignment2 fp = do
    pairs <- getAOCInput (many1 $ lexeme $ parsePair) fp
    return $ length $ filter overlapping' pairs

  main :: IO ()
  main = do
    args <- getArgs
    if (length $ filter (=="benchmark") args) == 1 then do
      withArgs (drop 1 args) $ criterion
    else do
      assignment1 "input.txt" >>= print
      assignment2 "input.txt" >>= print

  criterion :: IO ()
  criterion = defaultMain
    [
    bgroup "assignment1" $ [bench "" $ whnfIO (assignment1 "input.txt")],
    bgroup "assignment2" $ [bench "" $ whnfIO (assignment2 "input.txt")]
    ]
