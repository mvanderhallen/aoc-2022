{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Day2 where
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  data InputMove = A | B | C deriving (Eq,Show)
  data Strategy = X | Y | Z deriving (Eq, Show)

  strategyScore :: Strategy -> Int
  strategyScore X = 1
  strategyScore Y = 2
  strategyScore Z = 3

  gameScore :: InputMove -> Strategy -> Int
  gameScore A X = 3
  gameScore A Y = 6
  gameScore B Y = 3
  gameScore B Z = 6
  gameScore C Z = 3
  gameScore C X = 6
  gameScore _ _ = 0

  parseInputMove :: Parser InputMove
  parseInputMove = lexeme $
    symbol "A" *> return A
    <|>
    symbol "B" *> return B
    <|>
    symbol "C" *> return C

  parseStrategy :: Parser Strategy
  parseStrategy = lexeme $
    symbol "X" *> return X
    <|>
    symbol "Y" *> return Y
    <|>
    symbol "Z" *> return Z

  parseLine :: Parser (InputMove, Strategy)
  parseLine = (,) <$> parseInputMove <*> parseStrategy

  assignment1 :: IO ()
  assignment1 = do
    input <- getAOCInput (many $ lexeme parseLine) "input.txt"
    print $ sum $ map (\(a,b) -> gameScore a b + strategyScore b) input

  gameScore' :: Strategy -> Int
  gameScore' X = 0
  gameScore' Y = 3
  gameScore' Z = 6

  findMove :: InputMove -> Strategy -> InputMove
  findMove x Y = x
  findMove A X = C
  findMove A Z = B
  findMove B X = A
  findMove B Z = C
  findMove C X = B
  findMove C Z = A

  moveScore' :: InputMove -> Int
  moveScore' A = 1
  moveScore' B = 2
  moveScore' C = 3



  assignment2 :: IO ()
  assignment2 = do
   input <- getAOCInput (many $ lexeme parseLine) "input.txt"
   print $ sum $ map (\(a,b) -> gameScore' b + moveScore' (findMove a b)) input
