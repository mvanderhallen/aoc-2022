{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Data.Set (Set, toList, fromList, union)
  import qualified Data.Set as Set

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle
  import Data.List (sortBy, nub, foldl')

  type Cost = (Int, Int, Int)
  type Income = (Int, Int, Int, Int)
  type Treasure = (Int, Int, Int, Int)
  data Blueprint = MkBlueprint {ore :: Cost, clay :: Cost, obsidian :: Cost, geode :: Cost} deriving (Show, Eq)
  type PuzzleType = [Blueprint]

  (+++) :: Treasure -> Income -> Treasure
  (+++) (ore, clay, obsidian, geode) (i1, i2, i3, i4) = (ore+i1, clay+i2, obsidian+i3, geode+i4)

  (<->) :: Treasure -> Cost -> Treasure
  (<->) (ore, clay, obsidian, geode) (c1,c2,c3) = (ore-c1, clay-c2, obsidian-c3, geode)

  (>>>) ::  Treasure -> Cost -> Bool
  (>>>) (ore, clay, obsidian, _) (cOre, cClay, cObsidian) = and $ zipWith (>=) [ore, clay, obsidian] [cOre, cClay, cObsidian]

  data Plan = Ore | Clay | Obsidian | Geode | NoPlan deriving (Eq, Show, Ord)
  type PlanState = ((Int, Int, Int, Int), (Int, Int, Int, Int), Plan)

  multipleRounds :: Int -> [PlanState] -> Blueprint -> [PlanState]
  multipleRounds n seed bp = foldl' (\x f -> f x) seed (replicate n go)
    where
      go ::  [PlanState] -> [PlanState]
      -- go !list = foldl' (flip purge) [] $ Prelude.concatMap (\(x,y,z) -> doRound bp x y z) $ list-- go !list = foldl' (flip purge) [] $ Prelude.concatMap (\(x,y,z) -> doRound bp x y z) $ list
      go !list = Prelude.concatMap (\(x,y,z) -> doRound bp x y z) $ list

  purge :: ((Int, Int, Int, Int), (Int, Int, Int, Int), Plan) -> [((Int, Int, Int, Int), (Int, Int, Int, Int), Plan)] -> [((Int, Int, Int, Int), (Int, Int, Int, Int), Plan)]
  purge (t,i,p) [] = [(t,i,p)]
  purge (t,i,p) ((t',i',p'):ts)
    | (t,i,p) == (t',i',p') = (t,i,p) : ts
    | p == p' && better t t' && better i i' = purge (t,i,p) ts
    | p == p' && better t' t && better i' i = (t',i',p'):ts
    | otherwise = (t',i',p'): purge (t,i,p) ts

  better (a,b,c,d) (e,f,g,h) = and $ zipWith (>=) [a,b,c,d] [e,f,g,h]

  doRound :: Blueprint -> Treasure -> Income -> Plan -> [(Treasure, Income, Plan)]
  doRound bp treasure income plan = do
    plan' <- planBuy bp plan treasure income
    let (treasure', income', plan'') = buildIfPossible plan' income
    let treasure'' = treasure' +++ income
    return (treasure'', income', plan'')
    where
      buildIfPossible :: Plan -> Income -> (Treasure, Income, Plan)
      buildIfPossible NoPlan _ = (treasure, income, NoPlan)
      buildIfPossible Ore      (o,c,ob,g) = if treasure >>> ore bp then (treasure<->ore bp, (o+1,c,ob,g), NoPlan)
        else (treasure,income, Ore)
      buildIfPossible Clay     (o,c,ob,g) = if treasure >>> clay bp then (treasure<->clay bp, (o,c+1,ob,g), NoPlan)
        else (treasure,income, Clay)
      buildIfPossible Obsidian (o,c,ob,g) = if treasure >>> obsidian bp then (treasure<->obsidian bp, (o,c,ob+1,g),  NoPlan) else (treasure,income, Obsidian)
      buildIfPossible Geode    (o,c,ob,g) = if treasure >>> geode bp then (treasure<->geode bp, (o,c,ob,g+1), NoPlan)
      else (treasure,income, Geode)

  planBuy ::Blueprint -> Plan -> Treasure -> Income -> [Plan]
  planBuy bp NoPlan (to,tc,tob,tg) (io,ic,iob,ig) = map snd $ filter (fst)  [(planOre, Ore), (planClay, Clay), (planObs, Obsidian), (planGeo, Geode)]
    where
      planGeo = io > 0 && iob > 0
      planClay = ic <= maxClay
      planOre  = io <= maxOre
      planObs  = iob <= maxObs && io >0 && ic > 0
      (maxOre, maxClay, maxObs) = (\(a,b,c) -> (maximum a, maximum b, maximum c)) $ unzip3 $ [ore bp, clay bp, obsidian bp, geode bp]
  planBuy _ x  _ _     = return x

  inputParser :: Parser PuzzleType
  inputParser = many1 $ lexeme $ do
    symbol "Blueprint"
    natural
    symbol ": Each ore robot costs "
    cOre <- (,0,0) <$> int
    symbol "ore. Each clay robot costs "
    cClay <- (,0,0) <$> int
    bp1  <- symbol "ore. Each obsidian robot costs" *> int
    bp2 <- symbol "ore and" *> int
    g1 <- symbol "clay. Each geode robot costs " *> int
    g2 <- symbol "ore and " *> int
    symbol "obsidian."
    return $ MkBlueprint cOre cClay (bp1, bp2, 0) (g1, 0, g2)

  bp1 = MkBlueprint (4,0,0) (2,0,0) (3,14,0) (2,0,7)
  bp2 = MkBlueprint (2,0,0) (3,0,0) (3,8,0) (3,0,12)

  assignment1 :: PuzzleType -> Int
  assignment1 = sum . zipWith (*) [1..] . map (maximum . map (\((_,_,_,g),_,_)->g) . multipleRounds 24 ([((0,0,0,0),(1,0,0,0), NoPlan)]))

  assignment2 :: PuzzleType -> Int
  assignment2 = product . map (maximum . map (\((_,_,_,g),_,_)->g) . multipleRounds 32 ([((0,0,0,0),(1,0,0,0), NoPlan)])) . take 3

  assignment2' :: PuzzleType -> Int
  assignment2' = product . map (maximum . map (\((_,_,_,g),_,_)->g) . compute) . take 3
    where
      compute bp = multipleRounds 16 (foldr (purge) [] $ multipleRounds 16 [((0,0,0,0),(1,0,0,0), NoPlan)] bp) bp

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
