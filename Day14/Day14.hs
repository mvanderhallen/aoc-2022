{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import Control.Monad (replicateM, foldM)


  import Control.Monad.State
  import Data.HashSet (HashSet, member, insert, empty)
  import Data.Maybe (isJust)

  -- imports lexeme, symbol, integer, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle

  type RockLine = [(Int, Int)]

  parseRockLines :: Parser [RockLine]
  parseRockLines = many1 (lexeme $ parseRockLine)
    where
      parseRockLine :: Parser RockLine
      parseRockLine = sepBy1 ((,) <$> (int <* symbol ",") <*> int) (symbol "->")

  type PuzzleType = [RockLine]

  maxY :: [RockLine] -> Int
  maxY =  maximum . map snd. concat

  data ProblemConfig = ProblemConfig {
      isUnoccupied :: ProblemConfig -> (Int, Int) -> State (HashSet (Int,Int)) Bool,
      limit :: Int
  }

  settleSand :: ProblemConfig -> State (HashSet (Int,Int)) (Maybe (Int, Int))
  settleSand cfg = go (500,0)
    where
      go :: (Int, Int) -> State (HashSet (Int,Int)) (Maybe (Int, Int))
      go (x,y)
       | y > limit cfg = return Nothing
       | y <= limit cfg = do
          down <- isUnoccupied cfg cfg (x,y+1)
          if down then
            go (x,y+1)
          else do
            left <- isUnoccupied cfg cfg (x-1,y+1)
            if left then
              go (x-1,y+1)
            else do
              right <- isUnoccupied cfg cfg (x+1, y+1)
              if right then
                go (x+1, y+1)
              else do
                return <$> occupy (x,y)

  occupy :: (Int,Int) -> State (HashSet (Int,Int)) (Int, Int)
  occupy x = modify (insert x) >> return x

  countSettledWhile :: ProblemConfig -> (Maybe (Int, Int) -> Bool) -> State (HashSet (Int,Int)) Int
  countSettledWhile cfg cond = go 0
    where
      go :: Int -> State (HashSet (Int,Int)) Int
      go x = do
        continue <- settleSand cfg
        if cond continue
          then go (x+1)
          else return x

  assignment1 :: [RockLine] -> Int
  assignment1 rocklines = evalState (occupyRockLines rocklines >> countSettledWhile cfg isJust) empty
    where
      cfg = ProblemConfig { isUnoccupied = (\_ x -> gets (not . member x)), limit = maxY rocklines }

  occupyRockLines :: [RockLine] -> State (HashSet (Int,Int)) ()
  occupyRockLines = mapM_ occupyRockLine

  occupyRockLine :: [(Int, Int)] -> State (HashSet (Int,Int)) ()
  occupyRockLine [x] = return ()
  occupyRockLine ((x1,y1):(x2,y2):rocklines)
    | x1 == x2 = do
        mapM_ (\y -> occupy (x1,y)) [min y1 y2 .. max y1 y2]
        occupyRockLine $ (x2,y2):rocklines
    | y1 == y2 = do
        mapM_ (\x -> occupy (x,y1)) [min x1 x2 .. max x1 x2]
        occupyRockLine $ (x2,y2):rocklines

  unoccupied :: ProblemConfig -> (Int, Int) -> State (HashSet (Int,Int)) Bool
  unoccupied cfg (x,y)
    | y == limit cfg= return False
    | otherwise = gets (not . member (x,y))

  assignment2 :: [RockLine] -> Int
  assignment2 rocklines = 1 + evalState (occupyRockLines rocklines >> countSettledWhile cfg (/= Just (500,0))) empty
    where
      cfg = ProblemConfig { isUnoccupied = unoccupied, limit = 2 + maxY rocklines}

  instance Puzzle.Puzzle PuzzleType where
    parseInput = parseRockLines
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
