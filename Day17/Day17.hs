{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle

  import Control.Monad.State
  import Data.List

  data Polyomino = Space | Cross | ReverseL | Bar | Block deriving (Eq, Show, Ord)
  data Move = L | R deriving (Eq, Show, Ord)
  data AppState = MkState { occupied :: OccupiedCells, moves :: [Move], pointer :: Int, highest :: Int} deriving (Show, Eq, Ord)

  currentMove :: AppState -> Move
  currentMove (MkState{..}) = moves !! pointer

  incrementPointer :: AppState -> AppState
  incrementPointer s@(MkState {..}) = s {pointer = (pointer + 1) `mod` (length moves)}

  type OccupiedCells = [(Int, Int)]

  while :: Int -> [Polyomino] -> State AppState Int
  while i (p:xs) = do
    insertBlock p
    x <- gets pointer
    if x == 0 || i == 40000 then
      return i
    else
      while (i+1) xs

  insertBlock :: Polyomino -> State AppState (Int,Int)
  insertBlock p = do
      h <- gets (highest)
      (x,y) <- go p (3,h+4)
      occupy p (x,y)
      return (x,y)
    where
      go :: Polyomino -> (Int, Int) -> State AppState (Int,Int)
      go p (x,y) = do
        (x',y') <- doMove (x,y)
        b <- checkBlock p (x',y')
        let (x'',y'') = if b then (x,y) else (x',y')
        b' <- checkBlock p (x'', y''-1)
        if b' then
          return (x'',y'')
        else
          go p (x'',y''-1)

  occupy :: Polyomino -> (Int, Int) -> State AppState ()
  occupy Space (x,y) =
    modify (\st@(MkState{..})->st {occupied = [(x,y),(x+1,y), (x+2,y), (x+3,y)] ++ occupied, highest = max highest y})
  occupy Cross (x,y) =
    modify (\st@(MkState{..}) ->st {occupied = [(x,y+1), (x+1,y), (x+1,y+1), (x+1,y+2), (x+2,y+1)]++occupied, highest = max highest (y+2)})
  occupy ReverseL (x,y) =
    modify (\st@(MkState{..}) ->st {occupied = [(x,y), (x+1,y), (x+2,y), (x+2,y+1), (x+2,y+2)]++occupied, highest = max highest (y+2)})
  occupy Bar (x,y) =
    modify (\st@(MkState{..}) ->st {occupied = [(x,y), (x,y+1), (x,y+2), (x,y+3)]++occupied, highest = max highest (y+3)})
  occupy Block (x,y) =
    modify (\st@(MkState{..}) ->st {occupied = [(x,y), (x,y+1), (x+1,y), (x+1,y+1)]++occupied, highest = max highest (y+1)})

  doMove :: (Int, Int) -> State AppState (Int,Int)
  doMove (x,y) = do
    move <- gets currentMove
    modify incrementPointer
    case move of
      L -> return (x-1,y)
      R -> return (x+1,y)

  checkBlock :: Polyomino -> (Int, Int) -> State AppState Bool
  checkBlock _ (_, 0) = return True
  checkBlock Space (x,y) = do
    occupied <- gets occupied
    return $ x <= 0 || x > 4 || any (`elem` occupied) [(x,y),(x+1,y), (x+2,y), (x+3,y)]
  checkBlock Cross (x,y) = do
    occupied <- gets occupied
    return $ x <= 0 || x > 5 || any (`elem` occupied) [(x,y+1), (x+1,y), (x+1,y+1), (x+1,y+2), (x+2,y+1)]
  checkBlock ReverseL (x,y) = do
    occupied <- gets occupied
    return $ x <= 0 || x > 5 || any (`elem` occupied) [(x,y), (x+1,y), (x+2,y), (x+2,y+1), (x+2,y+2)]
  checkBlock Bar (x,y) = do
    occupied <- gets occupied
    return $ x <= 0 || x > 7 || any (`elem` occupied) [(x,y), (x,y+1), (x,y+2), (x,y+3)]
  checkBlock Block (x,y) = do
    occupied <- gets occupied
    return $ x <= 0 || x > 6 || any (`elem` occupied) [(x,y), (x,y+1), (x+1,y), (x+1,y+1)]

  type PuzzleType = [Move]

  inputParser :: Parser PuzzleType
  inputParser = undefined

  assignment1 :: [Move] -> Int
  assignment1 moves = highest $ execState (mapM insertBlock $ take 2022 $ concat $ repeat [Space, Cross, ReverseL, Bar, Block]) (MkState [] moves 0 0)

  assignment2 :: PuzzleType -> Int
  assignment2 moves = getByPeriod moves 1000000000000

  findPeriod :: [Move] -> Int -> [(Int,[Int])]
  findPeriod moves i = foldr join [] $ sort $ map (\(x,y)->(y,x)) $ occupied $ execState (mapM insertBlock $ take i $ concat $ repeat [Space, Cross, ReverseL, Bar, Block]) (MkState [] moves 0 0)
    where
      join (y,x) [] = [(y, [x])]
      join (y,x) ((y', elems) : xs)
        | y == y' = (y,sort (x:elems)):xs
        | otherwise = (y,[x]) : (y', elems) : xs
        -- after 122 blocks, pointer = 710, highest = 187, 1234567/2
        -- after 3602 blocks, pointer = 710, highest = 5619, 1234567/2
        -- after 7082 blocks, pointer = 710, highest = 11051, 1234567/2
        ---------------------------------------------------------------
        -- after 122 blocks, every 3480 blocks bring the pointer back to 720, add a height of 5432, and makes the bottom 1234567/2

  getByPeriod :: [Move] -> Int -> Int
  getByPeriod moves i = preambleHeight + periodicHeight + partialPeriodHeight - 1
    where
      preambleHeight = 187
      periodicHeight  = ((i-122) `div` 3480) * 5432
      partialPeriodHeight = highest $ execState (mapM insertBlock $ take toTake $ concat $ repeat [ReverseL, Bar, Block, Space, Cross]) (MkState [(2,1)] moves 710 1)
      toTake = ((i - 122) `mod` 3480)

  instance Puzzle.Puzzle PuzzleType where
    parseInput = return inputMoves
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType

  exampleMoves = [R,R,R,L,L,R,L,R,R,L,L,L,R,R,L,R,R,R,L,L,L,R,R,R,L,L,L,R,L,L,L,R,R,L,R,R,L,L,R,R]

  inputMoves = [R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,R,R,R,L,R,R,R,R,L,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,R,L,R,R,R,L,L,R,R,R,L,L,R,R,L,R,R,R,L,R,R,R,L,L,L,L,R,L,L,L,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,R,R,L,L,L,R,R,L,L,L,R,L,L,L,R,L,L,R,R,L,L,R,R,R,R,L,L,R,R,R,L,L,R,L,L,R,R,L,L,R,L,L,R,R,R,L,L,R,R,R,R,L,L,L,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,R,L,L,L,R,L,L,R,R,R,L,L,R,R,L,R,R,R,R,L,L,L,L,R,L,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,L,R,L,L,L,R,L,L,L,R,L,R,R,R,R,L,R,R,L,R,R,R,L,L,R,L,L,L,R,R,R,L,R,L,L,L,R,R,L,L,L,L,R,R,L,L,R,R,L,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,R,L,L,R,L,L,R,R,L,L,L,L,R,R,L,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,R,R,L,L,L,R,L,L,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,L,L,R,R,L,R,L,L,L,L,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,R,R,L,R,R,R,L,R,R,R,R,L,L,L,R,L,R,R,L,L,L,L,R,L,L,R,L,R,R,R,R,L,L,R,L,L,L,L,R,L,L,L,R,R,R,L,R,L,R,R,R,R,L,L,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,L,R,L,L,L,L,R,L,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,L,L,R,R,R,L,L,R,L,L,L,L,R,R,L,L,R,R,R,L,L,L,R,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,R,L,L,R,R,R,L,L,R,R,L,L,R,L,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,R,L,L,R,R,L,L,R,R,R,L,R,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,L,R,R,R,R,L,L,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,L,L,L,L,R,L,R,R,L,L,L,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,R,R,R,R,L,L,L,R,R,L,L,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,R,L,L,R,L,R,L,L,L,L,R,R,L,L,L,L,R,L,L,L,R,L,L,R,R,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,L,L,L,L,R,R,R,L,R,R,L,R,R,R,L,R,R,R,L,L,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,R,R,R,L,L,L,R,R,R,L,R,R,R,L,L,R,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,R,L,R,R,L,L,R,R,L,L,L,R,R,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,R,L,L,R,L,L,R,L,L,L,L,R,L,L,L,L,R,R,R,L,L,L,L,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,R,R,R,R,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,R,R,L,L,R,R,R,L,L,L,R,L,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,L,R,R,R,R,L,L,R,L,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,L,L,L,R,L,L,R,R,R,R,L,L,R,L,L,L,R,R,R,L,L,L,R,L,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,R,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,L,R,R,R,R,L,L,R,R,L,R,R,L,L,L,L,R,L,L,L,R,R,L,L,R,R,R,R,L,R,R,L,R,R,R,R,L,L,L,R,R,R,L,L,R,L,L,L,R,R,L,L,R,R,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,R,L,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,R,R,R,L,L,R,R,R,L,L,L,R,R,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,L,L,R,R,R,L,R,R,L,R,R,L,L,L,R,R,R,L,R,R,R,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,R,R,R,L,L,R,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,L,R,R,R,L,L,R,R,R,R,L,L,L,R,L,L,L,L,R,L,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,L,L,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,L,L,L,R,R,R,L,L,L,L,R,L,R,R,L,L,L,R,L,L,L,L,R,R,R,R,L,L,R,L,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,L,L,L,R,R,R,R,L,R,L,L,L,R,R,L,L,R,R,R,L,L,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,L,L,R,R,R,R,L,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,L,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,L,L,R,R,L,L,L,R,L,L,L,R,R,R,L,R,R,L,L,L,R,L,L,R,L,L,L,R,R,R,L,L,R,L,L,R,R,R,L,R,R,L,L,L,R,R,R,R,L,L,R,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,R,R,L,R,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,R,L,L,L,L,R,R,R,R,L,R,R,R,L,L,L,L,R,R,R,L,R,L,R,L,L,L,L,R,L,R,L,L,R,R,L,L,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,R,L,L,L,L,R,R,R,L,R,R,R,L,L,L,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,L,R,R,L,L,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,R,L,R,R,R,L,L,L,L,R,L,L,L,L,R,L,R,L,L,R,R,R,R,L,L,L,R,R,L,R,R,R,R,L,L,L,R,R,R,L,R,R,L,R,R,L,R,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,R,L,L,R,L,L,R,R,L,L,L,L,R,L,L,L,R,L,R,R,R,R,L,R,R,R,L,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,R,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,L,R,L,L,L,L,R,R,R,R,L,R,R,L,L,R,R,L,R,R,L,R,R,R,R,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,R,L,L,L,L,R,R,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,R,R,R,R,L,R,L,L,L,L,R,R,R,R,L,L,R,L,L,L,R,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,R,R,L,L,R,R,L,L,R,L,L,L,L,R,R,L,L,L,R,L,L,L,L,R,R,R,L,R,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,L,L,L,R,L,L,R,R,R,L,R,R,R,R,L,R,R,L,L,L,R,R,R,R,L,R,R,R,L,R,R,L,L,L,L,R,R,R,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,R,R,R,L,L,L,L,R,R,R,R,L,R,R,L,R,R,R,L,R,R,R,L,L,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,L,R,R,L,L,L,L,R,L,L,L,L,R,R,R,L,L,R,R,L,R,L,L,L,L,R,L,R,L,L,L,L,R,R,L,L,R,R,R,R,L,L,R,R,L,R,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,R,L,R,R,R,R,L,R,R,R,R,L,L,R,L,L,L,L,R,L,L,R,R,L,L,R,R,R,R,L,L,R,R,L,L,L,R,R,R,L,R,R,L,R,L,L,L,L,R,L,L,R,L,L,R,R,L,L,L,R,R,R,L,L,R,R,R,L,L,R,R,R,R,L,L,L,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,L,L,L,L,R,L,L,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,L,R,R,R,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,R,L,L,R,R,R,L,L,L,R,L,R,R,R,L,R,L,L,L,L,R,R,R,L,R,R,R,L,R,R,L,R,L,L,L,R,R,L,R,R,R,R,L,R,L,L,L,R,R,L,L,R,L,L,L,R,R,R,L,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,L,L,L,R,L,R,L,R,R,R,L,L,L,R,L,R,R,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,L,L,L,L,R,L,R,R,L,L,L,L,R,R,R,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,L,L,L,L,R,L,L,R,R,R,L,L,L,L,R,R,L,L,R,R,L,L,L,R,L,L,L,L,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,R,R,L,L,R,R,R,R,L,R,L,L,R,R,L,L,L,L,R,L,R,L,L,L,R,R,R,L,L,L,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,L,L,L,R,R,L,R,R,R,L,L,L,R,L,R,R,R,L,L,R,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,L,L,L,R,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,R,L,L,L,R,R,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,R,L,L,L,L,R,R,R,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,L,R,L,R,R,R,R,L,R,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,L,L,R,R,R,L,R,L,L,R,L,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,L,L,R,R,L,L,R,L,L,L,R,R,R,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,L,R,R,R,L,L,L,R,R,L,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,L,L,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,L,L,L,R,L,L,R,R,L,L,L,L,R,R,R,L,R,R,L,R,R,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,R,R,R,L,L,R,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,L,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,R,R,L,L,L,L,R,L,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,R,R,L,L,L,R,R,L,L,R,R,R,R,L,L,L,R,R,R,L,R,L,L,L,R,R,L,L,L,L,R,R,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,R,L,L,R,R,R,L,R,R,R,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,R,L,L,R,L,L,L,L,R,R,R,R,L,R,R,R,L,L,R,R,R,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,R,R,R,L,L,L,R,L,L,L,L,R,L,R,R,R,R,L,L,R,R,L,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,L,R,R,R,L,L,L,L,R,L,L,R,R,R,L,L,L,R,L,L,L,L,R,L,R,R,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,R,R,L,L,R,R,R,L,L,L,R,L,L,L,L,R,R,L,R,L,L,L,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,R,R,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,L,R,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,R,R,L,R,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,L,L,R,R,L,L,R,R,R,L,L,L,R,R,R,L,R,R,L,R,L,L,R,R,R,L,L,R,R,L,L,L,L,R,L,R,R,L,L,L,R,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,L,L,L,R,R,L,L,L,R,R,L,L,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,R,L,L,L,R,R,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,R,R,R,R,L,L,R,R,L,R,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,L,L,L,R,L,L,L,L,R,L,R,R,R,L,L,L,L,R,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,R,L,L,R,R,L,L,R,L,L,L,R,R,L,L,L,L,R,L,L,L,R,L,L,L,R,R,L,L,L,R,L,L,R,R,R,R,L,L,L,R,L,L,R,R,R,R,L,L,R,L,R,R,L,L,R,R,R,L,L,R,R,R,L,R,R,L,L,L,R,R,L,R,R,R,R,L,L,R,R,R,R,L,L,R,R,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,L,L,R,L,R,R,L,L,R,L,R,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,R,R,L,R,R,R,L,R,R,R,R,L,L,L,R,L,L,L,R,R,L,R,R,R,L,L,R,L,L,R,R,L,L,L,L,R,L,L,R,R,L,L,R,R,L,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,L,R,R,R,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,R,L,R,L,L,L,L,R,L,L,R,L,L,L,R,R,L,L,L,L,R,R,R,L,R,L,L,L,L,R,L,L,L,L,R,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,L,R,L,L,R,R,L,L,R,R,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,R,R,L,L,L,L,R,L,L,L,L,R,L,L,R,L,L,R,R,L,L,L,R,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,R,R,L,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,R,R,R,R,L,L,L,L,R,L,R,R,R,L,L,L,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,L,R,R,R,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,L,R,R,L,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,L,R,R,R,R,L,R,R,R,R,L,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,R,L,L,L,L,R,L,L,L,R,L,L,L,L,R,R,R,L,R,L,L,L,L,R,R,L,R,R,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,R,R,R,L,R,R,L,L,L,R,L,R,R,R,L,L,L,R,L,R,R,L,L,L,L,R,R,L,L,R,R,R,R,L,L,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,L,R,R,R,L,L,R,R,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,R,L,R,L,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,R,R,L,R,R,L,L,L,L,R,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,R,R,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,L,L,R,R,R,L,L,R,R,R,L,L,R,R,R,R,L,L,R,R,L,R,R,R,L,R,L,L,R,R,R,R,L,R,R,L,R,R,L,R,R,L,R,R,L,L,L,R,R,L,L,L,L,R,R,L,R,R,R,L,L,L,R,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,L,R,L,R,R,R,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,R,R,R,R,L,L,R,R,R,L,L,R,R,R,L,R,L,L,L,L,R,R,R,L,L,R,R,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,L,L,R,R,R,R,L,L,L,L,R,L,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,L,L,L,R,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,L,R,R,L,R,R,L,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,L,R,L,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,R,L,L,L,L,R,L,L,L,R,R,L,R,R,R,R,L,L,L,R,R,L,R,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,R,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,L,L,L,R,L,L,L,L,R,R,R,L,L,R,L,L,R,L,R,R,R,R,L,L,R,L,L,L,L,R,R,L,R,R,R,L,L,L,L,R,R,R,L,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,R,L,R,R,L,L,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,L,L,R,R,R,L,L,L,R,R,R,L,R,R,R,L,L,L,R,L,R,L,R,L,R,R,R,L,L,L,L,R,L,L,R,R,R,R,L,R,R,L,L,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,L,R,L,L,L,L,R,R,L,L,R,R,R,L,L,L,R,R,R,R,L,R,R,L,R,L,R,L,L,L,R,L,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,R,R,R,L,L,R,R,L,L,L,L,R,L,L,R,R,R,L,L,L,R,R,R,L,L,R,L,L,L,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,L,R,R,R,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,L,L,R,R,L,L,R,L,L,L,R,R,L,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,L,L,L,R,R,R,L,L,R,R,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,R,R,L,L,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,L,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,R,R,R,L,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,R,L,L,L,R,R,L,L,L,L,R,L,R,R,R,R,L,L,R,L,L,R,R,R,L,R,R,R,L,L,L,R,R,L,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,L,L,R,R,R,R,L,R,R,L,L,R,R,L,L,L,L,R,R,L,L,R,R,R,L,R,R,R,L,L,L,R,R,L,L,R,R,L,R,L,L,L,L,R,R,R,L,R,R,L,L,R,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,L,R,L,R,R,R,R,L,R,R,R,R,L,R,R,R,R,L,L,R,R,L,L,L,L,R,L,R,R,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,R,R,L,L,L,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,R,R,L,L,L,L,R,R,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,R,L,L,L,R,L,L,R,R,R,L,L,R,L,L,R,R,L,L,L,R,L,L,R,L,R,R,R,R,L,L,R,R,R,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,R,R,L,L,L,R,R,L,R,R,L,L,L,R,L,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,R,L,R,R,R,R,L,L,L,R,L,L,R,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,L,R,R,R,L,L,L,L,R,R,L,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,L,R,R,R,R,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,L,L,L,R,L,L,R,R,R,R,L,L,L,L,R,L,L,R,R,R,L,L,R,R,L,R,R,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,L,L,R,R,R,L,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,L,L,R,L,L,L,R,R,L,R,R,L,L,L,L,R,L,L,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,L,L,R,R,L,L,R,R,L,L,R,R,R,R,L,L,L,L,R,L,R,R,R,L,L,R,L,L,R,R,L,L,R,L,L,L,R,R,L,L,R,R,R,R,L,L,L,L,R,L,R,R,L,L,L,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,L,R,L,L,R,L,L,L,L,R,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,R,R,R,R,L,L,R,L,L,L,L,R,L,L,L,L,R,R,L,L,L,L,R,L,L,R,L,L,R,R,L,L,R,R,R,R,L,R,R,L,L,L,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,L,L,R,L,R,R,L,R,R,L,L,L,L,R,R,L,L,L,R,R,L,R,R,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,R,L,L,L,L,R,R,L,L,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,R,L,L,L,L,R,R,R,L,L,L,R,R,R,L,L,R,R,L,L,L,R,L,R,L,L,R,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,R,R,L,L,L,R,L,L,L,R,R,R,L,L,R,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,R,L,L,R,R,L,L,L,R,R,L,R,L,L,L,L,R,R,L,R,L,L,L,R,L,L,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,R,R,L,R,R,R,R,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,L,L,L,R,R,R,R,L,R,L,R,R,R,R,L,L,R,R,R,L,L,R,R,R,L,L,L,R,R,L,R,R,L,L,R,L,R,R,L,L,L,L,R,R,L,L,L,L,R,L,L,R,L,L,L,L,R,R,L,L,L,R,R,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,L,L,R,R,R,L,R,R,R,L,L,R,L,R,R,L,L,L,R,L,R,L,R,R,R,R,L,L,R,L,R,R,R,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,R,L,R,R,L,L,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,R,R,L,L,L,L,R,L,L,R,R,R,L,L,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,L,L,L,L,R,L,L,L,R,R,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,R,R,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,L,R,L,L,L,R,L,L,L,L,R,R,R,L,R,L,R,R,L,R,R,R,R,L,R,R,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,L,L,R,L,L,R,R,R,R,L,L,R,R,R,L,R,R,L,R,L,R,R,R,L,L,L,L,R,R,L,R,R,L,L,R,R,R,L,L,L,R,R,L,R,R,R,L,L,R,R,L,L,L,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,R,L,L,L,R,R,L,L,L,L,R,R,R,L,R,R,L,L,L,L,R,R,L,L,R,R,R,L,L,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,R,L,R,R,R,L,L,R,R,R,L,L,R,R,L,L,L,R,R,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,R,L,R,L,L,L,R,L,L,R,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,L,R,R,R,R,L,L,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,R,R,R,R,L,L,R,L,R,R,L,L,R,L,L,R,L,L,R,R,R,L,L,L,L,R,L,L,R,R,R,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,L,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,L,L,L,R,L,L,R,R,L,L,L,L,R,R,L,L,L,R,L,L,R,R,R,R,L,R,L,R,R,R,L,L,R,R,R,R,L,L,R,R,L,R,R,R,R,L,L,L,R,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,L,L,L,R,R,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,R,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,R,R,R,R,L,L,R,R,R,L,R,R,R,L,L,L,L,R,R,L,R,R,R,L,L,L,R,R,R,L,L,L,L,R,L,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,R,R,L,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,L,R,L,L,R,L,L,R,R,R,R,L,L,R,R,R,L,L,L,R,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,R,R,R,L,R,L,L,L,R,L,L,L,R,R,R,L,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,L,L,R,R,R,L,L,L,L,R,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,L,R,R,L,L,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,R,L,R,R,R,L,L,R,R,R,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,R,R,R,L,R,L,R,R,R,R,L,L,R,R,L,R,R,R,R,L,L,L,R,R,L,R,R,R,L,L,L,R,L,L,R,R,R,L,L,R,L,L,L,R,R,L,R,R,R,L,L,R,R,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,L,L,R,R,R,R,L,R,R,L,L,L,R,L,L,L,L,R,R,R,L,L,L,R,L,R,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,L,R,R,R,L,L,L,L,R,R,L,R,R,R,L,L,L,R,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,L,L,L,L,R,R,R,L,L,L,R,R,R,L,R,L,L,L,R,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,R,R,L,R,R,R,R,L,L,R,R,R,R,L,R,L,L,L,R,L,L,R,R,R,R,L,L,R,R,L,L,R,R,R,L,R,R,L,R,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,R,R,R,R,L,L,L,R,R,L,L,R,L,R,R,R,L,L,R,L,L,L,R,R,R,R,L,L,R,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,R,L,L,L,R,L,L,R,R,R,L,R,R,R,L,R,R,L,R,R,L,L,L,R,R,R,R,L,L,L,R,L,L,L,R,R,L,L,L,L,R,R,L,L,L,R,R,L,L,R,R,R,R,L,R,R,R,R,L,L,L,L,R,R,R,R,L,L,R,R,R,L,L,R,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,R,R,L,L,L,L,R,L,L,R,L,R,R,R,R,L,L,L,R,R,R,R,L,L,R,R,R,L,R,L,L,L,L,R,R,L,R,R,R,L,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,L,R,R,R,R,L,L,L,L,R,R,R,L,L,R,R,R,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,L,R,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,L,R,L,R,R,R,L,R,L,L,R,L,L,R,L,L,R,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,L,R,R,R,R,L,R,R,R,L,R,R,L,L,R,R,R,L,L,L,R,L,R,R,L,L,L,L,R,R,R,R,L,R,L,L,L,R,R,R,L,L,L,R,R,L,L,R,L,L,L,R,R,R,L,L,R,L,L,L,R,R,L,L,L,R,L,L,L,L,R,R,R,L,L,R,R,R,R,L,L,R,R,R,L,L,L,R,L,L,L,R,R,L,L,R,L,R,L,L,R,R,R,R,L,L,R,R,L,L,L,R,R,R,R,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,L,L,L,R,R,L,L,L,R,R,R,L,L,R,R,R,L,L,R,R,R,L,R,R,R,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,L,L,R,R,L,L,L,L,R,R,R,R,L,R,R,R,L,L,R,R,R,R,L,L,L,L,R,R,L,R,R,L,R,R,R,R,L,R,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,R,R,L,L,L,L,R,L,L,R,R,L,L,L,L,R,R,R,L,L,L,R,R,R,R,L,R,R,R,R,L,L,L,R,R,L,L,L,R,L,L,L,R,R,L,L,L,L,R,R,L,L,R,L,L,R,R,R,L,R,L,L,L,L,R,R,L,R,R,L,L,R,R,R,L,R,L,L,L,L,R,R,R,L,L,R,R,R,L,L,L,R,L,L,R,R,L,L,R,R,L,R,R,L,L,R,R,L,L,R,R,R,R,L,L,L,R,L,L,L,L,R,R,R,R,L,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,L,L,L,L,R,R,L,R,L,L,L,L,R,R,R,R,L,L,L,L,R,L,R,R,L,R,R,R,R,L,L,L,R,L,L,R,R,R,R,L,L,R,R,R,L,L,R,R,L,L,L,L,R,R,R,R,L,R,L,R,R,L,L,L,R,L,R,R,R,R,L,L,L,R,R,R,L,L,L,R,R,R,L,R,R,L,L,R,R,R,R,L,L,L,R,L,L,L,R,L,L,L,L,R,L,L,R,R,L,L,L,R,R,R,L,L,L,L,R,R,L,L,L,L,R,L,L,L,L,R,L,L,L,R,R,R,R,L,L,L,R,L,R,L,L,L,L,R,R,L,L,R,L,L,R,R,R]
