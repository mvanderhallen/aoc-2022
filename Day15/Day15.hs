{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, sortOn)
  import Data.Maybe (fromJust, catMaybes)

  import Data.HashMap.Strict (HashMap, empty, fromList, unionWith, lookup)
  import Control.Monad.State
    -- imports lexeme, symbol, integer, natural, int, signed, decimal, whiteSpace, Parser a,
    -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle

  data SensorInfo = MkSensorInfo { sensorPos :: (Int, Int), beaconPos :: (Int,Int) } deriving (Eq, Show)
  type PuzzleType = [SensorInfo]

  type Risk = Int
  type RiskMap = HashMap (Int,Int) Risk

  inputParser :: Parser PuzzleType
  inputParser = many1 $ lexeme parseSensor
    where
      parseSensor = do
        symbol "Sensor at x="
        sensorX <- signed int
        symbol ", y="
        sensorY <- signed int
        symbol ": closest beacon is at x="
        beaconX <- signed int
        symbol ", y="
        beaconY <- signed int
        return $ MkSensorInfo (sensorX, sensorY) (beaconX, beaconY)


  manhattan :: (Int, Int) -> (Int, Int) -> Int
  manhattan (x1,y1) (x2,y2) = sum [abs (x2-x1), abs (y2-y1)]

  projectOn :: SensorInfo -> Int -> Maybe (Int, Int)
  projectOn (MkSensorInfo {..}) row = if xrange >=0 then
        Just (fst sensorPos - xrange, fst sensorPos + xrange)
      else
        Nothing
    where
      dist = manhattan sensorPos beaconPos
      ydist = abs $ row - (snd sensorPos)
      xrange = (dist - ydist)

  -- -1 corrects for the beacon located on the row of interest. Derive this number from input!
  assignment1 :: [SensorInfo] -> Int
  assignment1 sensors = (-1) + (length $ filter (\x -> any (\(m1,m2) -> x >= m1 && x <= m2) forbidden) [minX..maxX])
    where
      forbidden = catMaybes $ map (flip projectOn 2000000) sensors
      (minX, maxX) = foldr1 (\(min1,max1) (min2,max2) -> (min min1 min2, max max1 max2)) forbidden

  forbiddenOnLine :: Int -> [SensorInfo] -> [(Int, Int)]
  forbiddenOnLine i = joinSegments . sort . catMaybes . map (flip projectOn i)

  joinSegments :: [(Int, Int)] -> [(Int, Int)]
  joinSegments [] = []
  joinSegments [(x,y)] = [(x,y)]
  joinSegments ((x,y):(x2,y2):xs) = if y2 <= y
    then
      joinSegments $ (x,y) : xs
    else if y >= x2 then
      joinSegments $ (x, y2) : xs
    else
      (x,y) : joinSegments ((x2,y2):xs)

  assignment2 :: [SensorInfo] -> Int
  assignment2 sensors =calcFreq $ head $ filter (\x -> length (snd x) > 1) $ map (\row -> (row, flip forbiddenOnLine sensors row)) [0..4000000]
    where
      calcFreq (x, (x1,x2):_) = x + (x2+1)*4000000
  -- neighbours :: Int -> Int -> (Int, Int) -> [(Int, Int)]
  -- neighbours w h (x,y) = do
  --   (dx,dy) <- [(1,0), (-1,0), (0,1), (0,-1)]
  --   guard $ x+dx >= 0
  --   guard $ x+dx < w
  --   guard $ y+dy >= 0
  --   guard $ y+dy < h
  --   return (x+dx, y+dy)
  --
  -- dijkstra :: PuzzleType -> State RiskMap Risk
  -- dijkstra grid = go [((0,0),0)]
  --   where
  --     width = length $ grid !! 0
  --     height = length grid
  --     nbs :: (Int, Int) -> [(Int, Int)]
  --     nbs = neighbours width height
  --     go :: [((Int, Int), Int)] -> State RiskMap Risk
  --     go (((x,y), accumRisk) : pos) = do
  --       let neighbours = nbs (x,y)
  --       if (width-1, height-1) `elem` neighbours
  --         then return $ accumRisk + (grid !! (height-1) !! (width-1))
  --       else do
  --         riskMap <- get
  --         let nbsRisk = catMaybes $ map (calcRisk riskMap) neighbours :: [((Int,Int), Int)]
  --         modify (unionWith (\v1 -> const v1) (fromList nbsRisk))
  --         go (mergeOn snd (sortOn snd nbsRisk) pos)
  --         where
  --           calcRisk :: RiskMap -> (Int,Int) -> Maybe ((Int,Int), Int)
  --           calcRisk rm (x',y') = case lookup (x',y') rm of
  --             Nothing -> Just ((x',y'),accumRisk + grid !! y' !! x')
  --             Just i  -> let newRisk = accumRisk + grid !! y' !! x' in
  --               if newRisk < i then
  --                 Just ((x',y'), newRisk)
  --               else
  --                 Nothing
  --
  -- mergeOn :: Ord b => (a->b) -> [a] -> [a] -> [a]
  -- mergeOn _ [] [] = []
  -- mergeOn _ [] bs = bs
  -- mergeOn _ as [] = as
  -- mergeOn f (a:as) (b:bs)
  --   | compare (f a) (f b) == GT = b : mergeOn f (a:as) bs
  --   | otherwise = a : mergeOn f as (b:bs)
  --
  -- assignment1 :: PuzzleType -> Int
  -- assignment1 grid = evalState (dijkstra grid) empty
  --
  -- printGrid :: PuzzleType -> String
  -- printGrid = unlines . map (concatMap show)
  --
  -- bigMap :: PuzzleType -> Int -> PuzzleType
  -- bigMap grid n = concatMap (conc . constructRow) [0..n]
  --   where
  --     augmentWith :: Int -> [[Int]]
  --     augmentWith x = map (map (\v -> rollover (v + x))) grid
  --     rollover x
  --       | x > 9 = rollover (x-9)
  --       | otherwise = x
  --     constructRow row = map (augmentWith) [row..row+n] :: [[[Int]]]
  --     conc :: [[[Int]]] -> [[Int]]
  --     conc g = map (\line -> concatMap (\n -> g!!n!!line) [0..(length g - 1)]) [0..(length grid)-1]
  --
  -- assignment2 :: PuzzleType -> Int
  -- assignment2 grid = assignment1 $ bigMap grid 4
  --
  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
