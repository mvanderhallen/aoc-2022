{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  -- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
  -- getAOCInput, interactive, criterion and doMain
  import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
  import qualified Puzzle as Puzzle

  type PuzzleType = ()

  inputParser :: Parser PuzzleType
  inputParser = undefined

  assignment1 :: PuzzleType -> Int
  assignment1 input = undefined

  assignment2 :: PuzzleType -> Int
  assignment2 input = undefined

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
